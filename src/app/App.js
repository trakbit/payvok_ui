import React, { PropTypes } from 'react';
import { Button, Drawer, List, ListItem, ListDivider, AppBar, Navigation, GithubIcon } from 'react-toolbox'; // Bundled component import
import { IndexLink, Link } from 'react-router';

export default class App extends React.Component {
  state = {
    active: false
  };

  handleToggle = () => {
    this.setState({ active: !this.state.active });
  };

  render() {
    return (
      <div>
        <AppBar title='PayXam'>
          <Navigation type='horizontal'>
            <Button icon='menu' floating primary mini onClick={this.handleToggle} />
          </Navigation>
        </AppBar>
        <Drawer active={this.state.active} onOverlayClick={this.handleToggle}>
          <List>
            <ListItem leftIcon='menu'><Link>Menu</Link></ListItem>
            <ListDivider />
            <ListItem leftIcon='dvr'><IndexLink to="/">Dashboard</IndexLink></ListItem>
            <ListItem leftIcon='business'><Link to="/company">Company</Link></ListItem>
            <ListItem leftIcon='person'><Link to="/employee">Employee</Link></ListItem>
            <ListItem leftIcon='play_arrow'><Link to="/payroll">Payroll</Link></ListItem>
            <ListItem leftIcon='assignment'><Link to="/">Report</Link></ListItem>
          </List>
        </Drawer>
        {this.props.children}
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.object.isRequired
};