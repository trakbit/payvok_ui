import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './App.js';
import Company from './components/company/company';
import Employee from './components/employee/employee';
import Dashboard from './components/dashboard';
import Payroll from './components/payroll/payroll';

export default (
    <Route path="/" component={App}>
        <IndexRoute component={Dashboard} />
        <Route path="company" component={Company} />
        <Route path="employee" component={Employee} />
        <Route path="payroll" component={Payroll} />
    </Route>
);