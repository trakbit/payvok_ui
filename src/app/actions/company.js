//get company data
export function companyFetchDataSuccess(companySource) {
    return {
        type: 'GET_COMPANY_DATA',
        companySource
    };
}

export function getCompanyData() {
    return (dispatch) => {

        fetch('http://localhost:57727/api/v1/company/')
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((companySource) => {
                for (let i = 0; i < companySource.length; i++) {
                    companySource[i].value = i;
                }
                return companySource;
            })
            .then((companySource) => dispatch(companyFetchDataSuccess(companySource)));
    };
}

//selected company in company list
export function companySelect(selected) {
    return {
        type: 'COMPANY_SELECTED',
        selected
    };
}

export function companySelectData(selected) {
    return (dispatch) => {
        dispatch(companySelect(selected));
    };
}

//open and close ADD_COMPANY_DIALOG
export function addCompanyDialogOpen(activeAddCompanyDialog) {
    return {
        type: 'ADD_COMPANY_DIALOG',
        activeAddCompanyDialog
    };
}

export function addCompanyDialog(activeAddCompanyDialog) {
    return (dispatch) => {
        dispatch(addCompanyDialogOpen(activeAddCompanyDialog));
    };
}

//open and close UPDATE_COMPANY_DIALOG
export function updateCompanyDialogOpen(activeUpdateCompanyDialog) {
    return {
        type: 'UPDATE_COMPANY_DIALOG',
        activeUpdateCompanyDialog
    };
}

export function updateCompanyDialog(activeUpdateCompanyDialog) {
    return (dispatch) => {
        dispatch(updateCompanyDialogOpen(activeUpdateCompanyDialog));
    };
}

// Load company data in updateForm
export function loadCompany(loadedCompany) {
    return (dispatch) => {
        dispatch(loadCompanySuccess(loadedCompany));
    };
}

export function loadCompanySuccess(loadedCompany) {
    return {
        type: 'LOAD_COMPANY',
        loadedCompany
    };
}


// add company
export function addCompany(company) {
    return (dispatch => {
        const url = 'http://localhost:57727/api/v1/Company';
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(company)
        })
            .then(() => dispatch(getCompanyData()));
    });
}

//update Company
export function updateCompany(loadedCompany) {
    return (dispatch => {
        const item = loadedCompany.CompanyId;
        const url = 'http://localhost:57727/api/v1/Company/' + item;
        fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(loadedCompany)
        })
            .then(() => dispatch(getCompanyData()));
    });
}


// delete Company
export function deleteCompany(companyId) {
    return (dispatch => {
        const url = 'http://localhost:57727/api/v1/Company';
        fetch(url + '/' + companyId, {
            method: 'DELETE'
        })
            .then(() => dispatch(getCompanyData()));
    });
}
