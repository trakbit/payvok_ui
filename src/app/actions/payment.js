import _ from 'lodash';

// get payment data
export function getPaymentData() {
    return (dispatch) => {
        fetch("http://localhost:57727/api/v1/payment/")
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((paymentSource) => {
                _.forEach(paymentSource, (paymentSource) => {
                    if (paymentSource.Frequency === 1) {
                        paymentSource.Payment_Frequency = "Weekly";
                    }
                    else if (paymentSource.Frequency === 2) {
                        paymentSource.Payment_Frequency = "Fortnightly";
                    }
                    else if (paymentSource.Frequency === 3) {
                        paymentSource.Payment_Frequency = "4-Weekly";
                    }
                    else if (paymentSource.Frequency === 4) {
                        paymentSource.Payment_Frequency = "Monthly";
                    }
                });
                return paymentSource;
            })
            .then((paymentSource) => dispatch(getPaymentDataSuccess(paymentSource)))
    };
}

export function getPaymentDataSuccess(paymentSource) {
    return {
        type: 'GET_PAYMENT',
        paymentSource
    }
}

// add Payment data

export function addPayment(payment) {
    return (dispatch) => {
        let url = 'http://localhost:57727/api/v1/payment';
        let processStatus = function (response) {
            if (response.status === 200 || response.status === 0) {
                return Promise.resolve(response)
            } else {

            }
        };
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payment)
        })
            .then(() => dispatch(getPaymentData()));
    }
}


// update payment data
export function updatePayment(data) {
    return (dispatch) => {
        let url = 'http://localhost:57727/api/v1/payment/' + data.EmployeeId;
        let processStatus = function (response) {
            if (response.status === 200 || response.status === 0) {
                return Promise.resolve(response)
            } else {

            }
        };
        fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(() => dispatch(getPaymentData()))
    }
}

// delete payment data
export function deletePayment(item) {
    return (dispatch) => {
        let url = 'http://localhost:57727/api/v1/Payment';
        fetch(url + '/' + item, {
            method: 'DELETE'
        })
            .then(() => dispatch(getPaymentData()));
    }
}


// set frequency
export function setFrequency(frequency) {
    return (dispatch) => {
        dispatch(setFrequencySuccess(frequency))
    }
}

export function setFrequencySuccess(frequency) {
    return {
        type: 'SET_FREQUENCY',
        frequency
    }
}

// payment frequency string
export function setPaymentFrequency(payment_frequency) {
    return (dispatch) => {
        dispatch(setPaymentFrequencySuccess(payment_frequency))
    }
}

export function setPaymentFrequencySuccess(payment_frequency) {
    return {
        type: 'SET_PAYMENT_FREQUENCY',
        payment_frequency
    }
}

//set set salary type - annual or periodic 
export function setSalary(salary) {
    return (dispatch) => {
        dispatch(setSalarySuccess(salary))
    }
}

export function setSalarySuccess(salary) {
    return {
        type: 'SET_SALARY',
        salary
    }
}

// set annual salary
export function setAnnual(annual) {
    return (dispatch) => {
        dispatch(setAnnualSuccess(annual))
    }
}

export function setAnnualSuccess(annual) {
    return {
        type: 'SET_ANNUAL',
        annual
    }
}

//set periodic salary
export function setPeriodic(periodic) {
    return (dispatch) => {
        dispatch(setPeriodicSuccess(periodic))
    }
}

export function setPeriodicSuccess(periodic) {
    return {
        type: 'SET_PERIODIC',
        periodic
    }
}

// set annual input
export function setAnnualInput(annualInput) {
    return (dispatch) => {
        dispatch(setAnnualInputSuccess(annualInput))
    }
}

export function setAnnualInputSuccess(annualInput) {
    return {
        type: 'SET_ANNUAL_INPUT',
        annualInput
    }
}

//set periodic input
export function setPeriodicInput(periodicInput) {
    return (dispatch) => {
        dispatch(setPeriodicInputSuccess(periodicInput))
    }
}

export function setPeriodicInputSuccess(periodicInput) {
    return {
        type: 'SET_PERIODIC_INPUT',
        periodicInput
    }
}

//set payment method
export function setMethod(method) {
    return (dispatch) => {
        dispatch(setMethodSuccess(method))
    }
}

export function setMethodSuccess(method) {
    return {
        type: 'SET_METHOD',
        method
    }
}

//set hourly wages
export function setHourly(hourly) {
    return (dispatch) => {
        dispatch(setHourlySuccess(hourly))
    }
}

export function setHourlySuccess(hourly) {
    return {
        type: 'SET_HOURLY',
        hourly
    }
}

//set daily working hours
export function setDaily(daily) {
    return (dispatch) => {
        dispatch(setDailySuccess(daily))
    }
}

export function setDailySuccess(daily) {
    return {
        type: 'SET_DAILY',
        daily
    }
}

// set Selected payment
export function setSelectedPayment(selectedPayment) {
    return (dispatch) => {
        dispatch(setSelectedPaymentSuccess(selectedPayment))
    }
}

export function setSelectedPaymentSuccess(selectedPayment) {
    return {
        type: 'SET_SELECTED_PAYMENT',
        selectedPayment
    }
}