import moment from 'moment';
import _ from 'lodash';


//get employee data
export function employeeFetchDataSuccess(employeeSource) {
    return {
        type: 'GET_EMPLOYEE_DATA',
        employeeSource
    };
}

export function getEmployeeData() {
    return (dispatch) => {

        fetch('http://localhost:57727/api/v1/employee/')
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((employeeSource) => {
                _.forEach(employeeSource, (employee) => {
                    employee.Address = employee.AddressLine1
                        + ' ' + employee.AddressLine2
                        + ' ' + employee.AddressLine3
                        + ' ' + employee.AddressLine4;
                    employee.DateOfBirth = moment(employee.DateOfBirth).format('MM/DD/YYYY');
                });
                return employeeSource;
            })
            .then((employeeSource) => dispatch(employeeFetchDataSuccess(employeeSource)));
    };
}

//set company selected in dropdown
export function setCompanySelected(companySelected) {
    return (dispatch) => {
        dispatch(setCompanySelectedSuccess(companySelected));
    };
}

export function setCompanySelectedSuccess(companySelected) {
    return {
        type: 'SET_COMPANY_SELECTED',
        companySelected
    };
}

//set payment frequency selected in dropdown
export function setFrequencySelected(frequencySelected) {
    return (dispatch) => {
        dispatch(setFrequencySelectedSuccess(frequencySelected));
    };
}

export function setFrequencySelectedSuccess(frequencySelected) {
    return {
        type: 'SET_FREQUENCY_SELECTED',
        frequencySelected
    };
}


//set selected employees filtered by company
export function setCompanyFilteredEmployees(companyFilteredEmployees) {
    return (dispatch) => {
        dispatch(setCompanyFilteredEmployeesSuccess(companyFilteredEmployees));
    };
}

export function setCompanyFilteredEmployeesSuccess(companyFilteredEmployees) {
    return {
        type: 'SET_COMPANY_FILTERED_EMPLOYEES',
        companyFilteredEmployees
    };
}

// set selected employees filtered by department
export function setDepartmentFilteredEmployees(departmentFilteredEmployees) {
    return (dispatch) => {
        dispatch(setDepartmentFilteredEmployeesSuccess(departmentFilteredEmployees));
    };
}

export function setDepartmentFilteredEmployeesSuccess(departmentFilteredEmployees) {
    return {
        type: 'SET_DEPARTMENT_FILTERED_EMPLOYEES',
        departmentFilteredEmployees
    };
}

// set selected employees filtered by pay frequency
export function setFrequencyFilteredEmployees(frequencyFilteredEmployees) {
    return (dispatch) => {
        dispatch(setFrequencyFilteredEmployeesSuccess(frequencyFilteredEmployees));
    };
}

export function setFrequencyFilteredEmployeesSuccess(frequencyFilteredEmployees) {
    return {
        type: 'SET_FREQUENCY_FILTERED_EMPLOYEES',
        frequencyFilteredEmployees
    };
}

//get company data for employee
export function companyFetchDataSuccess(companySourceForEmployee) {
    return {
        type: 'GET_COMPANY_DATA_FOR_EMPLOYEE',
        companySourceForEmployee
    };
}

export function getCompanyDataForEmployee() {
    return (dispatch) => {

        fetch('http://localhost:57727/api/v1/company/')
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((companySourceForEmployee) => {
                _.forEach(companySourceForEmployee, (value, index) => value.value = index);
                return companySourceForEmployee;
            })
            .then((companySourceForEmployee) => dispatch(companyFetchDataSuccess(companySourceForEmployee)))
            .then(() => dispatch(getEmployeeData()));
    };
}


// set setSelectedDepartmentDropdown
export function setSelectedDepartmentDropdown(selectedDepartmentDropdown) {
    return (dispatch => {
        dispatch(setSelectedDepartmentDropdownSuccess(selectedDepartmentDropdown));
    });
}

export function setSelectedDepartmentDropdownSuccess(selectedDepartmentDropdown) {
    return {
        type: 'SET_SELECTED_DEPARTMENT_DROPDOWN',
        selectedDepartmentDropdown
    };
}

//open and close ADD_EMPLOYEE_DIALOG
export function addEmployeeDialogOpen(activeAddEmployeeDialog) {
    return {
        type: 'ADD_EMPLOYEE_DIALOG',
        activeAddEmployeeDialog
    };
}

export function addEmployeeDialog(activeAddEmployeeDialog) {
    return (dispatch) => {
        dispatch(addEmployeeDialogOpen(activeAddEmployeeDialog));
    };
}

// handle DateOfBirth change
export function handleDateChange(dateOfBirth) {
    return (dispatch) => {
        dispatch(handleDateChangeSuccess(dateOfBirth));
    };
}

export function handleDateChangeSuccess(dateOfBirth) {
    return {
        type: 'HANDLE_DATE_CHANGE',
        dateOfBirth
    };
}

// handle Gender change
export function handleGenderChange(gender) {
    return (dispatch) => {
        dispatch(handleGenderChangeSuccess(gender));
    };
}

export function handleGenderChangeSuccess(gender) {
    return {
        type: 'HANDLE_GENDER_CHANGE',
        gender
    };
}

// add employee
export function addEmployee(employee) {
    return (dispatch => {
        const url = 'http://localhost:57727/api/v1/employee';
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(employee)
        })
            .then(() => dispatch(getCompanyDataForEmployee()));
    });
}

//update employee
export function updateEmployee(employee) {
    return (dispatch => {
        const url = 'http://localhost:57727/api/v1/Employee/' + employee.EmployeeId;
        fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(employee)
        })
            .then(() => dispatch(getCompanyDataForEmployee()));
    });
}

//delete employee
export function deleteEmployee(EmployeeId) {
    return (dispatch => {
        const url = 'http://localhost:57727/api/v1/Employee';
        fetch(url + '/' + EmployeeId, {
            method: 'DELETE'
        })
            .then(() => dispatch(getCompanyDataForEmployee()));

    });
}


//set Employee Selected

export function setEmployeeSelected(employeeSelected) {
    return (dispatch => {
        dispatch(setEmployeeSelectedSuccess(employeeSelected));
    });
}

export function setEmployeeSelectedSuccess(employeeSelected) {
    return {
        type: 'SET_EMPLOYEE_SELECTED',
        employeeSelected
    };
}


// Load employee data in update Employee form
export function loadEmployee(loadedEmployee) {
    return (dispatch) => {
        dispatch(loadEmployeeSuccess(loadedEmployee));
    };
}

export function loadEmployeeSuccess(loadedEmployee) {
    return {
        type: 'LOAD_EMPLOYEE',
        loadedEmployee
    };
}

//open and close UPDATE_EMPLOYEE_DIALOG
export function updateEmployeeDialog(activeUpdateEmployeeDialog) {
    return (dispatch) => {
        dispatch(updateEmployeeDialogOpen(activeUpdateEmployeeDialog));
    };
}

export function updateEmployeeDialogOpen(activeUpdateEmployeeDialog) {
    return {
        type: 'UPDATE_EMPLOYEE_DIALOG',
        activeUpdateEmployeeDialog
    };
}