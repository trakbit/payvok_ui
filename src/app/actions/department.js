import _ from 'lodash';
//get company data
export function companyFetchDataSuccess(companySource) {
    return {
        type: 'GET_COMPANY_DATA',
        companySource
    };
}

export function getCompanyData() {
    return (dispatch) => {

        fetch('http://localhost:57727/api/v1/company/')
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((companySource) => {
                _.forEach(companySource, (value, index) => value.value = index);
                return companySource;
            })
            .then((companySource) => dispatch(companyFetchDataSuccess(companySource)))
            .then(() => dispatch(getAllDepartment()));
    };
}

//get list of all departments
export function getAllDepartmentDataSuccess(allDepartment) {
    return {
        type: 'GET_ALL_DEPARTMENT',
        allDepartment
    };
}

export function getAllDepartment() {
    return (dispatch) => {

        fetch('http://localhost:57727/api/v1/department/')
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((allDepartment) => dispatch(getAllDepartmentDataSuccess(allDepartment)));
    };
}

//set department selected
export function setDepartmentSelected(departmentSelected) {
    return (dispatch) => {
        dispatch(setDepartmentselectedsuccess(departmentSelected));
    };
}

export function setDepartmentselectedsuccess(departmentSelected) {
    return {
        type: 'SET_DEPARTMENT_SELECTED',
        departmentSelected
    };
}

//get company specific department
export function setCompanySelectedDepartmentSuccess(companySelectedDepartment) {
    return {
        type: 'COMPANY_SELECTED_DEPARTMENT',
        companySelectedDepartment
    };
}

export function setCompanySelectedDepartment(companySelectedDepartment) {
    return (dispatch) => {
        dispatch(setCompanySelectedDepartmentSuccess(companySelectedDepartment));
    };
}

//selected Department
export function setSelectedDepartmentSuccess(selectedDepartment) {
    return {
        type: 'SELECTED_DEPARTMENT',
        selectedDepartment
    };
}

export function setSelectedDepartment(selectedDepartment) {
    return (dispatch) => {
        dispatch(setSelectedDepartmentSuccess(selectedDepartment));
    };
}

//open and close ADD_DEPARTMENT_DIALOG
export function addDepartmentDialogOpen(activeAddDepartmentDialog) {
    return {
        type: 'ADD_DEPARTMENT_DIALOG',
        activeAddDepartmentDialog
    };
}

export function addDepartmentDialog(activeAddDepartmentDialog) {
    return (dispatch) => {
        dispatch(addDepartmentDialogOpen(activeAddDepartmentDialog));
    };
}

//selected company in dropdown when adding new department
export function selectCompanyForDepartment(selectCompany) {
    return (dispatch) => {
        dispatch(selectCompanyForDepartmentSuccess(selectCompany));
    };
}

export function selectCompanyForDepartmentSuccess(selectCompany) {
    return {
        type: 'SELECT_COMPANY_FOR_DEPARTMENT',
        selectCompany
    };
}

//add department

export function addDepartment(department) {
    return (dispatch => {
        const url = 'http://localhost:57727/api/v1/department';
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(department)
        })
            .then(() => dispatch(getAllDepartment()));
    });
}


//delete department

export function deleteDepartment(DepartmentId) {
    return (dispatch => {
        const url = 'http://localhost:57727/api/v1/Department';
        fetch(url + '/' + DepartmentId, {
            method: 'DELETE'
        })
            .then(() => dispatch(getAllDepartment()));
    });
}

//open and close UPDATE_DEPARTMENT_DIALOG
export function updateDepartmentDialogOpen(activeUpdateDepartmentDialog) {
    return {
        type: 'UPDATE_DEPARTMENT_DIALOG',
        activeUpdateDepartmentDialog
    };
}

export function updateDepartmentDialog(activeUpdateDepartmentDialog) {
    return (dispatch) => {
        dispatch(updateDepartmentDialogOpen(activeUpdateDepartmentDialog));
    };
}

// set Company set company data in loaded state
export function setLoadedDepartment(loadedDepartment) {
    return (dispatch) => {
        dispatch(setLoadedDepartmentSuccess(loadedDepartment));
    };
}

export function setLoadedDepartmentSuccess(loadedDepartment) {
    return {
        type: 'SET_LOADED_DEPARTMENT',
        loadedDepartment
    };
}

//update department
export function updateDepartment(loadedDepartment) {
    return (dispatch => {
        const item = loadedDepartment.DepartmentId;
        const url = 'http://localhost:57727/api/v1/Department/' + item;
        fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(loadedDepartment)
        })
            .then(() => dispatch(getAllDepartment()));
    });
}

// set department data
export function setDepartmentData(departmentSource) {
    return (dispatch => {
        dispatch(setDepartmentDataSuccess(departmentSource));
    });
}

export function setDepartmentDataSuccess(departmentSource) {
    return {
        type: 'SET_DEPARTMENT_DATA',
        departmentSource
    };
}