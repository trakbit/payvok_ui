export function allDepartment(state = [], action) {
    switch (action.type) {
        case 'GET_ALL_DEPARTMENT':
            return Object.assign([], action.allDepartment);
        default:
            return state;
    }
}

export function companySelectedDepartment(state = [], action) {
    switch (action.type) {
        case 'COMPANY_SELECTED_DEPARTMENT':
            return Object.assign([] , action.companySelectedDepartment);
        default:
            return state;
    }
}

export function selectedDepartment(state = [], action) {
    switch (action.type) {
        case 'SELECTED_DEPARTMENT':
            return Object.assign([], action.selectedDepartment);
        default:
            return state;
    }
}

export function activeAddDepartmentDialog(state = false, action) {
    switch (action.type) {
        case 'ADD_DEPARTMENT_DIALOG':
            return !state
        default:
            return state;
    }
}

export function selectCompany(state = { value: 0 }, action) {
    switch (action.type) {
        case 'SELECT_COMPANY_FOR_DEPARTMENT':
            return Object.assign({}, action.selectCompany);
        default:
            return state;
    }
}

export function activeUpdateDepartmentDialog(state = false, action) {
    switch (action.type) {
        case 'UPDATE_DEPARTMENT_DIALOG':
            return !state
        default:
            return state;
    }
}

export function loadedDepartment(state = {}, action) {
    switch (action.type) {
        case 'SET_LOADED_DEPARTMENT':
            return Object.assign({}, action.loadedDepartment);
        default:
            return state;
    }
}

export function departmentSelected(state = { value: 0 }, action) {
    switch (action.type) {
        case 'SET_DEPARTMENT_SELECTED':
            return Object.assign({}, action.departmentSelected);;
        default:
            return state;
    }
}