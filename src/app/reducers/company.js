export function companySource(state = [], action) {
    switch (action.type) {
        case 'GET_COMPANY_DATA':
            return Object.assign([], action.companySource);
        default:
            return state;
    }
}

export function selected(state = [0], action) {
    switch (action.type) {
        case 'COMPANY_SELECTED':
            return Object.assign([], action.selected);
        default:
            return state;
    }
}

export function activeAddCompanyDialog(state = false, action) {
    switch (action.type) {
        case 'ADD_COMPANY_DIALOG':
            return !state
        default:
            return state;
    }
}

export function activeUpdateCompanyDialog(state = false, action) {
    switch (action.type) {
        case 'UPDATE_COMPANY_DIALOG':
            return !state;
        default:
            return state;
    }
}

export function loadedCompany(state = {}, action) {
    switch (action.type) {
        case 'LOAD_COMPANY':
            return Object.assign({}, action.loadedCompany);
        default:
            return state;
    }
}