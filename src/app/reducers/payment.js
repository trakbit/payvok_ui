export function frequency(state = { value: 1 }, action) {
    switch (action.type) {
        case 'SET_FREQUENCY':
            return Object.assign({}, action.frequency);
        default:
            return state;
    }
}

export function payment_frequency(state = { value: "Weekly" }, action) {
    switch (action.type) {
        case 'SET_PAYMENT_FREQUENCY':
            return Object.assign({}, action.payment_frequency);
        default:
            return state;
    }
}

export function salary(state = { value: "periodic" }, action) {
    switch (action.type) {
        case 'SET_SALARY':
            return Object.assign({}, action.salary);
        default:
            return state;
    }
}

export function annual(state = { value: 0 }, action) {
    switch (action.type) {
        case 'SET_ANNUAL':
            return Object.assign({}, action.annual);
        default:
            return state;
    }
}

export function periodic(state = { value: 0 }, action) {
    switch (action.type) {
        case 'SET_PERIODIC':
            return Object.assign({}, action.periodic);
        default:
            return state;
    }
}

export function annualInput(state = true, action) {
    switch (action.type) {
        case 'SET_ANNUAL_INPUT':
            return !state
        default:
            return state;
    }
}

export function periodicInput(state = false, action) {
    switch (action.type) {
        case 'SET_PERIODIC_INPUT':
            return !state
        default:
            return state;
    }
}

export function method(state = { value: 'credit_transfer' }, action) {
    switch (action.type) {
        case 'SET_METHOD':
            return Object.assign({}, action.method);
        default:
            return state;
    }
}

export function hourly(state = { value: 0 }, action) {
    switch (action.type) {
        case 'SET_HOURLY':
            return Object.assign({}, action.hourly);
        default:
            return state;
    }
}

export function daily(state = { value: 0 }, action) {
    switch (action.type) {
        case 'SET_DAILY':
            return Object.assign({}, action.daily);
        default:
            return state;
    }
}

export function paymentSource(state = [], action) {
    switch (action.type) {
        case 'GET_PAYMENT':
            return Object.assign([], action.paymentSource);
        default:
            return state;
    }
}

export function selectedPayment(state = [], action) {
    switch (action.type) {
        case 'SET_SELECTED_PAYMENT':
            return Object.assign([], action.selectedPayment);
        default:
            return state;
    }
}