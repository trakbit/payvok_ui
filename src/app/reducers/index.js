import { createStore, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { companySource, selected, activeAddCompanyDialog, activeUpdateCompanyDialog, loadedCompany } from './company';
import {
    allDepartment,
    companySelectedDepartment,
    selectedDepartment,
    activeAddDepartmentDialog,
    selectCompany,
    activeUpdateDepartmentDialog,
    loadedDepartment,
    departmentSelected
} from './department';

import {
    employeeSource,
    companySelected,
    companyFilteredEmployees,
    departmentFilteredEmployees,
    frequencyFilteredEmployees,
    companySourceForEmployee,
    departmentSource,
    selectedDepartmentDropdown,
    activeAddEmployeeDialog,
    dateOfBirth,
    gender,
    employeeSelected,
    activeUpdateEmployeeDialog,
    loadedEmployee,
    frequencySelected
} from './employee';

import {
    frequency,
    payment_frequency,
    salary, annual,
    periodic,
    annualInput,
    periodicInput,
    method,
    hourly,
    daily,
    paymentSource,
    selectedPayment
} from './payment';

export default combineReducers({

    //**COMPANY
    companySource,
    allDepartment,
    companySelectedDepartment,
    selectedDepartment,
    activeAddCompanyDialog,
    activeUpdateCompanyDialog,
    activeUpdateDepartmentDialog,
    activeAddDepartmentDialog,
    loadedCompany,
    loadedDepartment,
    selected,
    selectCompany,
    form: formReducer,

    //**EMPLOYEE
    employeeSource,
    companySelected,
    frequencySelected,
    departmentSelected,
    companyFilteredEmployees,
    departmentFilteredEmployees,
    frequencyFilteredEmployees,
    companySourceForEmployee,
    departmentSource,
    selectedDepartmentDropdown,
    activeAddEmployeeDialog,
    activeUpdateEmployeeDialog,
    dateOfBirth,
    gender,
    employeeSelected,
    loadedEmployee,

    //*PAYMENT
    frequency,
    payment_frequency,
    salary,
    annual,
    periodic,
    annualInput,
    periodicInput,
    method,
    hourly,
    daily,
    paymentSource,
    selectedPayment
});