export function employeeSource(state = [], action) {
    switch (action.type) {
        case 'GET_EMPLOYEE_DATA':
            return Object.assign([], action.employeeSource);
        default:
            return state;
    }
}

export function companySelected(state = { value: 0 }, action) {
    switch (action.type) {
        case 'SET_COMPANY_SELECTED':
            return Object.assign({}, action.companySelected);
        default:
            return state;
    }
}

export function frequencySelected(state = { value: 0 }, action) {
    switch (action.type) {
        case 'SET_FREQUENCY_SELECTED':
            return Object.assign({}, action.frequencySelected);
        default:
            return state;
    }
}

export function companyFilteredEmployees(state = [], action) {
    switch (action.type) {
        case 'SET_COMPANY_FILTERED_EMPLOYEES':
            return Object.assign([], action.companyFilteredEmployees);
        default:
            return state;
    }
}


export function departmentFilteredEmployees(state = [], action) {
    switch (action.type) {
        case 'SET_DEPARTMENT_FILTERED_EMPLOYEES':
            return Object.assign([], action.departmentFilteredEmployees);
        default:
            return state;
    }
}

export function frequencyFilteredEmployees(state = [], action) {
    switch (action.type) {
        case 'SET_FREQUENCY_FILTERED_EMPLOYEES':
            return Object.assign([], action.frequencyFilteredEmployees);
        default:
            return state;
    }
}

export function companySourceForEmployee(state = [], action) {
    switch (action.type) {
        case 'GET_COMPANY_DATA_FOR_EMPLOYEE':
            return Object.assign([], action.companySourceForEmployee);
        default:
            return state;
    }
}


export function departmentSource(state = [], action) {
    switch (action.type) {
        case 'SET_DEPARTMENT_DATA':
            return Object.assign([], action.departmentSource);
        default:
            return state;
    }
}

export function selectedDepartmentDropdown(state = { value: 0 }, action) {
    switch (action.type) {
        case 'SET_SELECTED_DEPARTMENT_DROPDOWN':
            return Object.assign([], action.selectedDepartmentDropdown);
        default:
            return state;
    }
}

export function activeAddEmployeeDialog(state = false, action) {
    switch (action.type) {
        case 'ADD_EMPLOYEE_DIALOG':
            return !state
        default:
            return state;
    }
}

export function dateOfBirth(state = { value: new Date() }, action) {
    switch (action.type) {
        case 'HANDLE_DATE_CHANGE':
            return Object.assign({}, action.dateOfBirth)
        default:
            return state;
    }
}

export function gender(state = { value: 'male' }, action) {
    switch (action.type) {
        case 'HANDLE_GENDER_CHANGE':
            return Object.assign({}, action.gender)
        default:
            return state;
    }
}

export function employeeSelected(state = [0], action) {
    switch (action.type) {
        case 'SET_EMPLOYEE_SELECTED':
            return Object.assign([], action.employeeSelected);
        default:
            return state;
    }
}

export function activeUpdateEmployeeDialog(state = false, action) {
    switch (action.type) {
        case 'UPDATE_EMPLOYEE_DIALOG':
            return !state;
        default:
            return state;
    }
}

export function loadedEmployee(state = {}, action) {
    switch (action.type) {
        case 'LOAD_EMPLOYEE':
            return Object.assign({}, action.loadedEmployee);
        default:
            return state;
    }
}