import React from 'react';
import { Button, IconButton, Dropdown, Tabs, Tab, Table, Navigation, Dialog, section, Input } from 'react-toolbox';
import 'whatwg-fetch';

const PayrollButton = (props) => {
    return (
        <table>
            <tbody>
                <tr>
                    <td>
                        <Navigation
                            type='horizontal'>
                            <Button
                                label='Pay'
                                raised
                                primary
                                icon='fast_forward' />
                            <Button
                                label='Rewind'
                                raised
                                primary
                                icon='fast_rewind' />
                            <Button
                                label='Show periods'
                                raised
                                primary
                                onClick={props.toggleSidebar}
                                icon='today' />
                        </Navigation>
                    </td>
                    <td>
                        <Dropdown
                            label="Company"
                            auto
                            onChange={props.handleChange}
                            source={countries}
                            value={props.value}
                            />
                    </td>
                    <td>
                        <Dropdown
                            label="Department"
                            auto
                            onChange={props.handleChange}
                            source={countries}
                            value={props.value}
                            />
                    </td>
                    <td>
                        <Dropdown
                            label="Pay Frequency"
                            auto
                            onChange={props.handleChange}
                            source={countries}
                            value={props.value}
                            />
                    </td>
                </tr>
            </tbody>
        </table>
    );
}

const countries = [
    { value: 'EN-gb', label: 'Company 1' },
    { value: 'ES-es', label: 'Company 2' },
    { value: 'TH-th', label: 'Company 3' },
    { value: 'EN-en', label: 'Company 4' }
];

export default PayrollButton;