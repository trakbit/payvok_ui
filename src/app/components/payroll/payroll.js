import React from 'react';
import { Button, Navigation, IconButton, Dropdown, Table, Card, Layout, NavDrawer, Panel, Sidebar, Checkbox } from 'react-toolbox';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { connect } from 'react-redux';

import PayrollButton from './payrollButton';
import {
    getEmployeeData,
    setCompanySelected,
    setSelectedEmployees,
    getCompanyDataForEmployee,
    setDepartmentData,
    setSelectedDepartmentDropdown,
    addEmployeeDialog,
    handleDateChange,
    addEmployee,
    handleGenderChange,
    setEmployeeSelected,
    deleteEmployee,
    updateEmployeeDialog,
    loadEmployee,
    updateEmployee
} from '../../actions/employee';

import {
    setFrequency,
    setPaymentFrequency,
    setSalary,
    setAnnual,
    setPeriodic,
    setAnnualInput,
    setPeriodicInput,
    setMethod,
    setHourly,
    setDaily,
    getPaymentData,
    setSelectedPayment,
    addPayment,
    deletePayment,
    updatePayment
} from '../../actions/payment';

import { getAllDepartment } from '../../actions/department';

export class Payroll extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            activeAddPaymentDialog: false,
            activeUpdatePaymentDialog: false,
            index: 0,
            selected: [],
            source: periods,
            drawerActive: false,
            drawerPinned: false,
            sidebarPinned: false,
            value: 'ES-es'
        }
    }

    componentDidMount() {
        this.props.getPaymentData();
        this.props.getCompanyDataForEmployee();
        this.props.getDepartmentData();

    };

    componentWillReceiveProps(nextProps) {
        ///********** */
        if (this.props.employeeSource.length != nextProps.employeeSource.length) {
            if (this.props.companySourceForEmployee.length === 0) {
            }
            else {
                let selectedCompanyId = this.props.companySourceForEmployee[this.props.companySelected.value].CompanyId;
                let selectedDepartments = _.filter(this.props.allDepartment, { 'CompanyId': selectedCompanyId });
                //Preparing Dropdown List of departments after filtering company
                let departmentData = selectedDepartments;
                for (let i = 0; i < departmentData.length; i++) {
                    departmentData[i].value = i;
                }
                let selectedEmployees = _.filter(nextProps.employeeSource, { 'CompanyId': selectedCompanyId });
                this.props.setSelectedEmployees(selectedEmployees)
                this.props.setDepartmentData(selectedDepartments)
            }
        }
        ///********** */
    }

    handleChange = (row, key, value) => {
        const source = this.state.source;
        source[row][key] = value;
        this.setState({ source });
    };

    handleChange = (value) => {
        this.setState({ value: value });
    };

    handleEmployeeSelect = (employeeSelected) => {
        this.props.setEmployeeSelected(employeeSelected);
    }

    toggleSidebar = () => {
        this.setState({ sidebarPinned: !this.state.sidebarPinned });
    };



    render() {
        return (
            <Layout>
                <Panel>
                    <div style={{ flex: 1, overflowY: 'auto', padding: '1.8rem' }}>
                        <PayrollButton
                            toggleSidebar={this.toggleSidebar} 
                            handleChange={this.handleChange}
                            value={this.value}/>
                        <div style={{ padding: '1em' }}>
                            <Card style={{
                                'backgroundColor': 'WhiteSmoke',
                                'overflow': 'scroll',
                                'height': '30em'
                            }}>
                                <Table
                                    multiSelectable={false}
                                    onSelect={this.handleEmployeeSelect}
                                    selected={this.props.employeeSelected}
                                    model={EmployeeModel}
                                    source={this.props.selectedEmployees}
                                    />
                            </Card>
                        </div>
                        <Navigation
                            type='horizontal'>
                            <Button
                                label='Add'
                                raised
                                primary
                                icon='add_circle_outline' />
                            <Button
                                label='Remove'
                                raised
                                primary
                                icon='remove_circle_outline' />
                            <Button
                                label='Update'
                                raised
                                primary
                                icon='unarchive' />
                        </Navigation>
                        <div style={{ padding: '1em' }}>
                            <Card style={{
                                'backgroundColor': 'WhiteSmoke',
                                'overflow': 'scroll',
                                'height': '20em'
                            }}>
                                <Table
                                    model={Elements}
                                    onChange={this.handleChange}
                                    onSelect={this.handleSelect}
                                    selectable
                                    multiSelectable={false}
                                    selected={this.state.selected}
                                    source={this.state.elementSource}
                                    />
                            </Card>
                        </div>
                    </div>
                </Panel>
                <Sidebar pinned={this.state.sidebarPinned} width={5}>
                    <div><IconButton icon='close' onClick={this.toggleSidebar} /></div>
                    <div style={{ flex: 1 }}>
                        <Table
                            model={PayModel}
                            onChange={this.handleChange}
                            selectable
                            multiSelectable={false}
                            selected={this.state.selected}
                            source={this.state.source}
                            />
                    </div>
                </Sidebar>
            </Layout>
        );
    }
}

const PayModel = {
    Paye_period: { type: String },
    Pay_date: { type: String }
};

const periods = [
    { Paye_period: '1', Pay_date: '4th Jan 2016' },
    { Paye_period: '2', Pay_date: '11th Jan 2016' },
    { Paye_period: '3', Pay_date: '11th Jan 2016' },
    { Paye_period: '4', Pay_date: '11th Jan 2016' },
    { Paye_period: '5', Pay_date: '11th Jan 2016' },
    { Paye_period: '6', Pay_date: '11th Jan 2016' },
    { Paye_period: '7', Pay_date: '11th Jan 2016' },
    { Paye_period: '8', Pay_date: '11th Jan 2016' },
    { Paye_period: '9', Pay_date: '11th Jan 2016' },
    { Paye_period: '10', Pay_date: '11th Jan 2016' },
    { Paye_period: '11', Pay_date: '11th Jan 2016' },
    { Paye_period: '12', Pay_date: '11th Jan 2016' },
];


const EmployeeModel = {
    EmployeeId: { type: String },
    FirstName: { type: String },
    LastName: { type: String },
    MiddleName: { type: String },
    Address: { type: String },
    DateOfBirth: { type: String },
    Gender: { type: String },
    Email: { type: String }
};

const employeeSource = [
    { EmployeeId: '1', FirstName: 'harsh', LastName: 'vardhan', MiddleName: 'sharma', Address: 'd-29', DateOfBirth: '18/07/1993', Gender: 'Male', Email: 'harsh@gmail.com' },
    { EmployeeId: '2', FirstName: 'harsh', LastName: 'vardhan', MiddleName: 'sharma', Address: 'd-29', DateOfBirth: '18/07/1993', Gender: 'Male', Email: 'harsh@gmail.com' },
    { EmployeeId: '3', FirstName: 'harsh', LastName: 'vardhan', MiddleName: 'sharma', Address: 'd-29', DateOfBirth: '18/07/1993', Gender: 'Male', Email: 'harsh@gmail.com' },
    { EmployeeId: '4', FirstName: 'harsh', LastName: 'vardhan', MiddleName: 'sharma', Address: 'd-29', DateOfBirth: '18/07/1993', Gender: 'Male', Email: 'harsh@gmail.com' },
    { EmployeeId: '5', FirstName: 'harsh', LastName: 'vardhan', MiddleName: 'sharma', Address: 'd-29', DateOfBirth: '18/07/1993', Gender: 'Male', Email: 'harsh@gmail.com' },
    { EmployeeId: '6', FirstName: 'harsh', LastName: 'vardhan', MiddleName: 'sharma', Address: 'd-29', DateOfBirth: '18/07/1993', Gender: 'Male', Email: 'harsh@gmail.com' },
    { EmployeeId: '7', FirstName: 'harsh', LastName: 'vardhan', MiddleName: 'sharma', Address: 'd-29', DateOfBirth: '18/07/1993', Gender: 'Male', Email: 'harsh@gmail.com' },
    { EmployeeId: '7', FirstName: 'harsh', LastName: 'vardhan', MiddleName: 'sharma', Address: 'd-29', DateOfBirth: '18/07/1993', Gender: 'Male', Email: 'harsh@gmail.com' },
    { EmployeeId: '8', FirstName: 'harsh', LastName: 'vardhan', MiddleName: 'sharma', Address: 'd-29', DateOfBirth: '18/07/1993', Gender: 'Male', Email: 'harsh@gmail.com' },
    { EmployeeId: '9', FirstName: 'harsh', LastName: 'vardhan', MiddleName: 'sharma', Address: 'd-29', DateOfBirth: '18/07/1993', Gender: 'Male', Email: 'harsh@gmail.com' }]

const Elements = {
    Description: { type: String },
    Units: { type: String },
    Rate: { type: String },
    Amount: { type: String }
};

const elementSource = [
    { Description: 'Insurance', Units: '1', Rate: '2', Amount: '2' },
    { Description: 'Pension', Units: '1', Rate: '2', Amount: '2' },
    { Description: 'Benefits', Units: '1', Rate: '2', Amount: '2' },
    { Description: 'Bonus', Units: '1', Rate: '2', Amount: '2' },
    { Description: 'Insurance', Units: '1', Rate: '2', Amount: '2' },
];




const mapStateToProps = (state) => {
    return {
        //PAYMENT
        paymentSource: state.paymentSource,
        selectedPayment: state.selectedPayment,
        method: state.method,
        salary: state.salary,
        payment_frequency: state.payment_frequency,
        frequency: state.frequency,
        hourly: state.hourly,
        daily: state.daily,
        annual: state.annual,
        periodic: state.periodic,
        annualInput: state.annualInput,
        periodicInput: state.periodicInput,
        //EMPLOYEE
        loadedEmployee: state.loadedEmployee,
        employeeSelected: state.employeeSelected,
        gender: state.gender,
        dateOfBirth: state.dateOfBirth,
        companySourceForEmployee: state.companySourceForEmployee,
        employeeSource: state.employeeSource,
        allDepartment: state.allDepartment,
        departmentSource: state.departmentSource,
        companySelected: state.companySelected,
        selectedEmployees: state.selectedEmployees,
        selectedDepartmentDropdown: state.selectedDepartmentDropdown,
        activeAddEmployeeDialog: state.activeAddEmployeeDialog,
        activeUpdateEmployeeDialog: state.activeUpdateEmployeeDialog
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setDateOfBirth: (dateOfBirth) => dispatch(setDateOfBirth(dateOfBirth)),
        addEmployeeData: (data) => dispatch(addEmployee(data)),
        handleDateChange: (value) => dispatch(handleDateChange(value)),
        handleGenderChange: (value) => dispatch(handleGenderChange(value)),
        getCompanyDataForEmployee: () => dispatch(getCompanyDataForEmployee()),
        getEmployeeData: () => dispatch(getEmployeeData()),
        getDepartmentData: () => dispatch(getAllDepartment()),
        setDepartmentData: (departmentSource) => dispatch(setDepartmentData(departmentSource)),
        setCompanySelected: (companySelected) => dispatch(setCompanySelected(companySelected)),
        setSelectedEmployees: (selectedEmployees) => dispatch(setSelectedEmployees(selectedEmployees)),
        setEmployeeSelected: (employeeSelected) => dispatch(setEmployeeSelected(employeeSelected)),
        setSelectedDepartmentDropdown: (value) => dispatch(setSelectedDepartmentDropdown(value)),
        addEmployeeDialog: (activeAddEmployeeDialog) => dispatch(addEmployeeDialog(activeAddEmployeeDialog)),
        deleteEmployee: (EmployeeId) => dispatch(deleteEmployee(EmployeeId)),
        loadEmployee: (loadedEmployee) => dispatch(loadEmployee(loadedEmployee)),
        updateEmployeeDialog: (activeUpdateEmployeeDialog) => dispatch(updateEmployeeDialog(activeUpdateEmployeeDialog)),
        updateEmployee: (employee) => dispatch(updateEmployee(employee)),
        setFrequency: (frequency) => dispatch(setFrequency(frequency)),
        setPaymentFrequency: (payment_frequency) => dispatch(setPaymentFrequency(payment_frequency)),
        setSalary: (salary) => dispatch(setSalary(salary)),
        setPeriodic: (periodic) => dispatch(setPeriodic(periodic)),
        setAnnual: (annual) => dispatch(setAnnual(annual)),
        setAnnualInput: (annualInput) => dispatch(setAnnualInput(annualInput)),
        setPeriodicInput: (periodicInput) => dispatch(setPeriodicInput(periodicInput)),
        setMethod: (method) => dispatch(setMethod(method)),
        setHourly: (hourly) => dispatch(setHourly(hourly)),
        setDaily: (daily) => dispatch(setDaily(daily)),
        getPaymentData: () => dispatch(getPaymentData()),
        setSelectedPayment: (selectedPayment) => dispatch(setSelectedPayment(selectedPayment)),
        addPayment: (payment) => dispatch(addPayment(payment)),
        deletePayment: (EmployeeId) => dispatch(deletePayment(EmployeeId)),
        updatePayment: (data) => dispatch(updatePayment(data))
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Payroll);