import React from 'react';
import { Dialog } from 'react-toolbox';
import 'whatwg-fetch';
import AddCompanyForm from './addCompanyForm';

const AddCompanyModal = (props) => {
    return (
        <Dialog
            active={props.activeAddCompanyDialog}
            onEscKeyDown={props.addCompanyDialog}
            onOverlayClick={props.addCompanyDialog}
            title='Add Company Information'
            >
            <AddCompanyForm
                onSubmit={props.onSubmit}
                addCompanyDialog={props.addCompanyDialog} />
        </Dialog>
    );
};
export default AddCompanyModal;