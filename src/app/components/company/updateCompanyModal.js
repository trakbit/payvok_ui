import React from 'react';
import { Dialog } from 'react-toolbox';
import 'whatwg-fetch';
import UpdateCompanyForm from './updateCompanyForm';

const UpdateCompanyModal = (props) => {
    return (
        <Dialog
            active={props.activeUpdateCompanyDialog}
            onEscKeyDown={props.updateCompanyDialog}
            onOverlayClick={props.updateCompanyDialog}
            title='Update Company Information'
            >
            <UpdateCompanyForm
                loadedCompany={props.loadedCompany}
                onSubmit={props.onSubmit}
                updateCompanyDialog={props.updateCompanyDialog} />
        </Dialog>
    );
}

export default UpdateCompanyModal;