import React from 'react';
import { Button, Navigation } from 'react-toolbox';
import 'whatwg-fetch';

const CompanySubButton = (props) => {
    return (
        <Navigation
            type='horizontal'>
            <Button
                label='Add'
                raised
                primary
                onClick={props.addSubDialog}
                icon='add_circle_outline' />
            <Button
                label='Remove'
                raised
                primary
                onClick={props.deleteSubData}
                icon='remove_circle_outline' />
            <Button
                label='Update'
                raised
                primary
                onClick={props.updateSubDialog}
                icon='unarchive' />
        </Navigation>
    )
}

export default CompanySubButton;