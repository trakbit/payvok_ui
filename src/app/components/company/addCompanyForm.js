import React from 'react';
import { Button, Navigation, section, Input } from 'react-toolbox';
import 'whatwg-fetch';
import { Field, reduxForm } from 'redux-form';

const AddCompanyForm = (props) => {
    const {handleSubmit} = props;

    const email = value =>
        value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
            'Invalid email address' : undefined;
    const required = value => value ? undefined : 'Required';

    const companyNameField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='business'
                label='Company Name'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const tradeNameField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='business'
                label='Trade Name'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const employerNoField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='import_contacts'
                label='Employer Number'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const telephoneField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='import_contacts'
                label='Telephone'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const faxField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='print'
                label='Fax'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const emailField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='mail'
                label='Email'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    return (
        <section>
            <form onSubmit={handleSubmit}>
                <Field
                    component={companyNameField}
                    name="Name"
                    validate={required} />
                <Field
                    component={tradeNameField}
                    name="Trade_Name"
                    validate={required} />
                <Field
                    component={employerNoField}
                    name="Employer_No"
                    validate={required} />
                <Field
                    component={telephoneField}
                    name="Telephone"
                    validate={required} />
                <Field
                    component={faxField}
                    name="Fax"
                    validate={required} />
                <Field
                    component={emailField}
                    name="Email"
                    type="email"
                    validate={[email, required]} />
            </form>
            <Navigation type='horizontal'>
                <Button
                    label='Save'
                    raised
                    primary
                    onClick={handleSubmit} />
                <Button
                    label='Cancel'
                    raised
                    primary
                    onClick={props.addCompanyDialog} />
            </Navigation>
        </section>
    );
};

export default reduxForm({
    form: 'company'
})(AddCompanyForm);