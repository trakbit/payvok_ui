import React from 'react';
import { Button, Navigation } from 'react-toolbox';
import 'whatwg-fetch';

const CompanyButton = (props) => {
    return (
        <Navigation
            type='horizontal'>
            <Button
                label='Add'
                raised
                primary
                onClick={props.addCompanyDialog}
                icon='add_circle_outline' />
            <Button
                label='Remove'
                raised
                primary
                onClick={props.deleteData}
                icon='remove_circle_outline' />
            <Button
                label='Update'
                raised
                primary
                onClick={props.updateCompanyDialogAndLoadCompany}
                icon='unarchive' />
        </Navigation>
    );
}
export default CompanyButton;