import React from 'react';
import { Card, Tabs, Tab, Table, CardTitle } from 'react-toolbox';
import 'whatwg-fetch';
import _ from 'lodash';

import AddCompanyModal from './addCompanyModal';
import UpdateCompanyModal from './updateCompanyModal';
import CompanyButton from './companyButton';
import CompanySubButton from './companySubButton';
import AddDepartmentModal from './department/addDepartmentModal';
import UpdateDepartmentModal from './department/updateDepartmentModal';

import { connect } from 'react-redux';
import { companySelectData, addCompanyDialog, updateCompanyDialog, addCompany, deleteCompany, updateCompany, loadCompany } from '../../actions/company';
import { getCompanyData, getAllDepartment, setCompanySelectedDepartment, setSelectedDepartment, addDepartmentDialog, selectCompanyForDepartment, addDepartment, deleteDepartment, updateDepartmentDialog, setLoadedDepartment, updateDepartment } from '../../actions/department';

const CompanyModel = {
    CompanyId: { type: String },
    Name: { type: String },
    Trade_Name: { type: String },
    Employer_No: { type: String },
    Telephone: { type: String },
    Fax: { type: String },
    Email: { type: String }
};

const DepartmentModel = {
    DepartmentId: { type: String },
    Department_Name: { type: String }
};

export class Company extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            index: 0
        };
    }

    componentDidMount() {
        this.props.getCompanyData();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.allDepartment !== nextProps.allDepartment) {
            if (this.props.companySource.length !== 0) {
                const selectedCompanyId = this.props.companySource[this.props.selected[0]].CompanyId;
                const companySelectedDepartments = _.filter(nextProps.allDepartment, { 'CompanyId': selectedCompanyId });
                this.props.setCompanySelectedDepartment(companySelectedDepartments);
                this.props.setSelectedDepartment([]);
            }
        }
    }

    addSubDialog = () => {
        if (this.state.index === 0) {
            this.props.addDepartmentDialog();
        }
    }

    updateCompanyDialogAndLoadCompany = () => {
        const loadedCompany = this.props.companySource[this.props.selected[0]];
        this.props.loadCompany(loadedCompany);
        this.props.updateCompanyDialog();
    }

    updateSubDialog = () => {
        if (this.state.index === 0) {
            if (this.props.selectedDepartment.length !== 0) {
                const loadedDepartment = this.props.companySelectedDepartment[this.props.selectedDepartment[0]];
                this.props.setLoadedDepartment(loadedDepartment);
                this.props.updateDepartmentDialog();
            }
        }
    }

    handleDepartmentFormChange = (value) => {
        this.props.selectCompanyForDepartment({ value: value });
    };

    handleTabChange = (index) => {
        this.setState({ index });
    };

    companySelectData = (selected) => {
        this.props.companySelectData(selected);
        const selectedCompanyId = this.props.companySource[selected[0]].CompanyId;
        const companySelectedDepartments = _.filter(this.props.allDepartment, { 'CompanyId': selectedCompanyId });
        this.props.setCompanySelectedDepartment(companySelectedDepartments);
        this.props.setSelectedDepartment([]);
    };

    handleFormChange = (name, event) => {
        const change = {};
        change[name] = event;
        this.setState(change);
    };

    handleSubmit = (name, data) => {
        if (name === 'addCompany') {
            this.props.addCompany(data);
            this.props.addCompanyDialog();
        }
        else if (name === 'updateCompany') {
            this.props.updateCompany(data);
            this.props.updateCompanyDialog();
        }
        else if (name === 'addDepartment') {
            data.CompanyId = this.props.companySource[this.props.selectCompany.value].CompanyId;
            this.props.addDepartment(data);
            this.addSubDialog();
        }
        else if (name === 'updateDepartment') {
            this.props.updateDepartment(data);
            this.updateSubDialog();
        }
    }


    deleteData = () => {
        const companyId = this.props.companySource[this.props.selected[0]].CompanyId;
        this.props.deleteCompany(companyId);
    }

    deleteSubData = () => {
        if (this.state.index === 0) {
            if (this.props.companySelectedDepartment > 0) {
                const DepartmentId = this.props.companySelectedDepartment[this.props.selectedDepartment[0]].DepartmentId;
                this.props.deleteDepartment(DepartmentId);
            }
        }
    }

    render() {
        return (
            <div style={{ flex: 1, overflowY: 'auto', padding: '1.8rem' }}>
                <CompanyButton
                    addCompanyDialog={this.props.addCompanyDialog}
                    deleteData={this.deleteData}
                    updateCompanyDialogAndLoadCompany={this.updateCompanyDialogAndLoadCompany} />

                <div style={{ padding: '1em' }}>
                    <Card style={{
                        'backgroundColor': 'WhiteSmoke',
                        'overflow': 'scroll',
                        'height': '30em'
                    }}>
                        <CardTitle title="Company" />
                        <Table
                            multiSelectable={false}
                            model={CompanyModel}
                            onSelect={this.companySelectData}
                            selected={this.props.selected}
                            source={this.props.companySource}
                            />
                    </Card>
                </div>

                <CompanySubButton
                    addSubDialog={this.addSubDialog}
                    deleteSubData={this.deleteSubData}
                    updateSubDialog={this.updateSubDialog} />

                <div style={{ padding: '1em' }}>
                    <Card style={{ 'backgroundColor': 'WhiteSmoke' }}>
                        <Tabs
                            index={this.state.index}
                            onChange={this.handleTabChange}>
                            <Tab label='Departments'>
                                <Table
                                    multiSelectable={false}
                                    model={DepartmentModel}
                                    onSelect={this.props.setSelectedDepartment}
                                    selected={this.props.selectedDepartment}
                                    source={this.props.companySelectedDepartment}
                                    />
                            </Tab>
                            <Tab label='Pension Schemes'>
                                Pension Schemes
                            </Tab>
                            <Tab label='Savings Schemes'>
                                Savings Schemes
                            </Tab>
                            <Tab label='Bank Accounts'>
                                Bank Accounts
                            </Tab>
                        </Tabs>
                    </Card>
                </div>

                <AddCompanyModal
                    activeAddCompanyDialog={this.props.activeAddCompanyDialog}
                    addCompanyDialog={this.props.addCompanyDialog}
                    onSubmit={this.handleSubmit.bind(this, 'addCompany')} />

                <UpdateCompanyModal
                    activeUpdateCompanyDialog={this.props.activeUpdateCompanyDialog}
                    updateCompanyDialog={this.props.updateCompanyDialog}
                    loadedCompany={this.props.loadedCompany}
                    onSubmit={this.handleSubmit.bind(this, 'updateCompany')} />

                <AddDepartmentModal
                    onSubmit={this.handleSubmit.bind(this, 'addDepartment')}
                    activeAddDepartmentDialog={this.props.activeAddDepartmentDialog}
                    companySource={this.props.companySource}
                    addSubDialog={this.addSubDialog}
                    handleDepartmentFormChange={this.handleDepartmentFormChange}
                    selectCompany={this.props.selectCompany} />

                <UpdateDepartmentModal
                    onSubmit={this.handleSubmit.bind(this, 'updateDepartment')}
                    activeUpdateDepartmentDialog={this.props.activeUpdateDepartmentDialog}
                    companySource={this.props.companySource}
                    loadedDepartment={this.props.loadedDepartment}
                    updateSubDialog={this.updateSubDialog} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        company: state.company,
        loadedCompany: state.loadedCompany,
        loadedDepartment: state.loadedDepartment,
        companySource: state.companySource,
        selected: state.selected,
        allDepartment: state.allDepartment,
        companySelectedDepartment: state.companySelectedDepartment,
        selectedDepartment: state.selectedDepartment,
        selectCompany: state.selectCompany,
        activeAddCompanyDialog: state.activeAddCompanyDialog,
        activeAddDepartmentDialog: state.activeAddDepartmentDialog,
        activeUpdateCompanyDialog: state.activeUpdateCompanyDialog,
        activeUpdateDepartmentDialog: state.activeUpdateDepartmentDialog
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        getCompanyData: () => dispatch(getCompanyData()),
        companySelectData: (selected) => dispatch(companySelectData(selected)),
        getDepartmentData: () => dispatch(getAllDepartment()),
        setCompanySelectedDepartment: (selectedDepartments) => dispatch(setCompanySelectedDepartment(selectedDepartments)),
        setSelectedDepartment: (departmentSelected) => dispatch(setSelectedDepartment(departmentSelected)),
        addCompanyDialog: (activeAddCompanyDialog) => dispatch(addCompanyDialog(activeAddCompanyDialog)),
        addDepartmentDialog: (activeAddDepartmentDialog) => dispatch(addDepartmentDialog(activeAddDepartmentDialog)),
        updateCompanyDialog: (activeUpdateCompanyDialog) => dispatch(updateCompanyDialog(activeUpdateCompanyDialog)),
        updateDepartmentDialog: (activeUpdateDepartmentDialog) => dispatch(updateDepartmentDialog(activeUpdateDepartmentDialog)),
        setLoadedDepartment: (loadedDepartment) => dispatch(setLoadedDepartment(loadedDepartment)),
        loadCompany: (loadedCompany) => dispatch(loadCompany(loadedCompany)),
        addCompany: (company) => dispatch(addCompany(company)),
        addDepartment: (department) => dispatch(addDepartment(department)),
        updateCompany: (loadedCompany) => dispatch(updateCompany(loadedCompany)),
        deleteCompany: (CompanyId) => dispatch(deleteCompany(CompanyId)),
        updateDepartment: (loadedDepartment) => dispatch(updateDepartment(loadedDepartment)),
        selectCompanyForDepartment: (value) => dispatch(selectCompanyForDepartment(value)),
        deleteDepartment: (DepartmentId) => dispatch(deleteDepartment(DepartmentId))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Company);