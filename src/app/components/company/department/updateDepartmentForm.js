import React from 'react';
import { connect } from 'react-redux'
import { Button, IconButton, Dropdown, Tabs, Tab, Table, Navigation, Dialog, section, Input } from 'react-toolbox';
import 'whatwg-fetch';
import { Field, reduxForm } from 'redux-form';

class UpdateDepartmentForm extends React.Component {
    constructor(props) {
        super(props);
        const {handleSubmit} = props;
    }

    componentDidMount() {
        this.props.initialize(this.props.loadedDepartment)
    };


    customItem = (item) => {
        const containerStyle = {
            display: 'flex',
            flexDirection: 'row'
        };

        const contentStyle = {
            display: 'flex',
            flexDirection: 'column',
            flexGrow: 2
        };

        return (
            <div style={containerStyle}>
                <div style={contentStyle}>
                    <strong>{item.CompanyId}</strong>
                    <small>{item.Name}</small>
                </div>
            </div>
        );
    }

    DepartmentNameField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='business'
                label='Department Name'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    render() {
        return (
            <section>
                <form onSubmit={this.handleSubmit}>
                    <Field
                        component={this.DepartmentNameField}
                        name="Department_Name"
                        />
                </form>
                <Navigation type='horizontal'>
                    <Button
                        label='Save'
                        raised
                        primary
                        onClick={this.props.handleSubmit} />
                    <Button
                        label='Cancel'
                        raised
                        primary
                        onClick={this.props.updateSubDialog} />
                </Navigation>
            </section>
        );
    }
}



export default reduxForm({
    form: 'department',  // a unique identifier for this form
})(UpdateDepartmentForm)