import React from 'react';
import { connect } from 'react-redux'
import { Button, IconButton, Dropdown, Tabs, Tab, Table, Navigation, Dialog, section, Input } from 'react-toolbox';
import 'whatwg-fetch';
import { Field, reduxForm } from 'redux-form';

const DepartmentForm = (props) => {
    const {handleSubmit} = props;

    const CompanyIdField = ({ select, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Dropdown {...select}
                icon='business'
                auto={false}
                source={props.companySource}
                onChange={props.handleDepartmentFormChange}
                label='Select Company'
                value={props.selectCompany.value}
                template={customItem} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const DepartmentNameField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='business'
                label='Department Name'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    return (
        <section>
            <form onSubmit={handleSubmit}>
                <Field
                    component={CompanyIdField}
                    name="CompanyId"
                    value={props.selectCompany.value}
                    />
                <Field
                    component={DepartmentNameField}
                    name="Department_Name"
                    />
            </form>
            <Navigation type='horizontal'>
                <Button
                    label='Save'
                    raised
                    primary
                    onClick={props.handleSubmit} />
                <Button
                    label='Cancel'
                    raised
                    primary
                    onClick={props.addSubDialog} />
            </Navigation>
        </section>
    );
}

const customItem = (item) => {
    const containerStyle = {
        display: 'flex',
        flexDirection: 'row'
    };

    const contentStyle = {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 2
    };

    return (
        <div style={containerStyle}>
            <div style={contentStyle}>
                <strong>{item.CompanyId}</strong>
                <small>{item.Name}</small>
            </div>
        </div>
    );
}



export default reduxForm({
    form: 'department',  // a unique identifier for this form
})(DepartmentForm)