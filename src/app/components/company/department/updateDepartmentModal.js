import React from 'react';
import { Button, IconButton, Tabs, Tab, Table, Navigation, Dialog, section, Input } from 'react-toolbox';
import 'whatwg-fetch';
import UpdateDepartmentForm from './updateDepartmentForm';

const UpdateDepartmentModal = (props) => {
    return (
        <Dialog
            active={props.activeUpdateDepartmentDialog}
            onEscKeyDown={props.updateSubDialog}
            onOverlayClick={props.updateSubDialog}
            title='Update Department Information'
            >
            <UpdateDepartmentForm
                onSubmit={props.onSubmit}
                companySource={props.companySource}
                handleDepartmentFormChange={props.handleDepartmentFormChange}
                selectCompany={props.selectCompany}
                updateSubDialog={props.updateSubDialog}
                loadedDepartment={props.loadedDepartment} />
        </Dialog>
    );
}
export default UpdateDepartmentModal;