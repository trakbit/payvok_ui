import React from 'react';
import { Button, IconButton, Tabs, Tab, Table, Navigation, Dialog, section, Input } from 'react-toolbox';
import 'whatwg-fetch';
import DepartmentForm from './departmentForm';

const AddDepartmentModal = (props) => {
    return (
        <Dialog
            active={props.activeAddDepartmentDialog}
            onEscKeyDown={props.addSubDialog}
            onOverlayClick={props.addSubDialog}
            title='Add Department Information'
            >
            <DepartmentForm
                onSubmit={props.onSubmit}
                companySource={props.companySource}
                handleDepartmentFormChange={props.handleDepartmentFormChange}
                selectCompany={props.selectCompany}
                addSubDialog={props.addSubDialog} />
        </Dialog>
    );
}
export default AddDepartmentModal;