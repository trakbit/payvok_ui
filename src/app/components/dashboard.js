import React from 'react';
import { Button } from 'react-toolbox';

export default class Dashboard extends React.Component {
    render() {
        return (
            <div style={{ flex: 1, overflowY: 'auto', padding: '1.8rem' }}>
                <Button icon='bookmark' label='Dashboard' raised primary />
            </div>
        );
    }
}