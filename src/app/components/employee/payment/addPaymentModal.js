import React from 'react';
import { Button, IconButton, Tabs, Tab, Table, Navigation, Dialog, section, Input } from 'react-toolbox';
import 'whatwg-fetch';
import AddPaymentForm from './addPaymentForm';

const AddPaymentModal = (props) => {
    return (
        <Dialog
            active={props.activeAddPaymentDialog}
            onEscKeyDown={props.addSubDialog}
            onOverlayClick={props.addSubDialog}
            title='Add Payment Information'
            >
            <AddPaymentForm
                frequency={props.frequency}
                payment_frequency={props.payment_frequency}
                hourly={props.hourly}
                daily={props.daily}
                annual={props.annual}
                salary={props.salary}
                method={props.method}
                periodic={props.periodic}
                annualInput={props.annualInput}
                periodicInput={props.periodicInput}
                handleFormChange={props.handleFormChange} />
            <Navigation
                type='horizontal'>
                <Button
                    label='Save'
                    raised
                    primary
                    onClick={props.savePayment} />
                <Button
                    label='Cancel'
                    raised
                    primary
                    onClick={props.addSubDialog} />
            </Navigation>
        </Dialog>
    )
}

export default AddPaymentModal;