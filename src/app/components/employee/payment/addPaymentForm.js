import React from 'react';
import { Button, IconButton, Dropdown, Input, RadioGroup, RadioButton } from 'react-toolbox';

const AddPaymentForm = (props) => {

    const frequencies = [
        { value: 1, frequency: 'Weekly' },
        { value: 2, frequency: 'Fortnightly' },
        { value: 3, frequency: '4-Weekly' },
        { value: 4, frequency: 'Monthly' }
    ];

    const methods = [
        { value: 'cheque', method: 'Cheque' },
        { value: 'cash', method: 'Cash' },
        { value: 'credit_transfer', method: 'Credit Transfer' }
    ];

    const salaries = [
        { value: 'annual', method: 'Annual' },
        { value: 'periodic', method: 'Periodic' }
    ];

    const frequencyDropdown = (item) => {
        const containerStyle = {
            display: 'flex',
            flexDirection: 'row'
        };

        const contentStyle = {
            display: 'flex',
            flexDirection: 'column',
            flexGrow: 2
        };

        return (
            <div style={containerStyle}>
                <div style={contentStyle}>
                    <strong>{item.frequency}</strong>
                </div>
            </div>
        );
    }


    const methodDropdown = (item) => {
        const containerStyle = {
            display: 'flex',
            flexDirection: 'row'
        };

        const contentStyle = {
            display: 'flex',
            flexDirection: 'column',
            flexGrow: 2
        };

        return (
            <div style={containerStyle}>
                <div style={contentStyle}>
                    <strong>{item.method}</strong>
                </div>
            </div>
        );
    }

    const salaryDropdown = (item) => {
        const containerStyle = {
            display: 'flex',
            flexDirection: 'row'
        };

        const contentStyle = {
            display: 'flex',
            flexDirection: 'column',
            flexGrow: 2
        };

        return (
            <div style={containerStyle}>
                <div style={contentStyle}>
                    <strong>{item.method}</strong>
                </div>
            </div>
        );
    }

    return (
        <section>
            <Dropdown
                auto={false}
                source={frequencies}
                onChange={props.handleFormChange.bind(this, 'frequency')}
                label='Payment Frequency'
                template={frequencyDropdown}
                value={props.frequency.value}
                />
            <Dropdown
                auto={false}
                source={methods}
                onChange={props.handleFormChange.bind(this, 'method')}
                label='Payment Method'
                template={methodDropdown}
                value={props.method.value}
                />
            <Dropdown
                auto={false}
                source={salaries}
                onChange={props.handleFormChange.bind(this, 'salary')}
                label='Annual or Periodic Pay'
                template={salaryDropdown}
                value={props.salary.value}
                />
            <Input
                icon='monetization_on'
                type='number'
                label='Annual Salary'
                disabled={props.annualInput}
                value={props.annual.value}
                onChange={props.handleFormChange.bind(this, 'annual')} />
            <Input
                icon='monetization_on'
                type='number'
                label={'Periodic Payment(Euros) - ' + props.payment_frequency.value}
                disabled={props.periodicInput}
                value={props.periodic.value}
                onChange={props.handleFormChange.bind(this, 'periodic')} />
            <Input
                icon='monetization_on'
                type='number'
                label='Hourly Pay'
                value={props.hourly.value}
                onChange={props.handleFormChange.bind(this, 'hourly')} />
            <Input
                icon='schedule'
                type='number'
                label='Daily Hours Worked'
                value={props.daily.value}
                onChange={props.handleFormChange.bind(this, 'daily')} />
        </section>
    );
}

export default AddPaymentForm;