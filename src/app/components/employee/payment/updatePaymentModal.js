import React from 'react';
import { Button, IconButton, Tabs, Tab, Table, Navigation, Dialog, section, Input } from 'react-toolbox';
import 'whatwg-fetch';
import UpdatePaymentForm from './updatePaymentForm';

const UpdatePaymentModal = (props) => {
    return (
        <Dialog
            active={props.activeUpdatePaymentDialog}
            onEscKeyDown={props.updateSubDialog}
            onOverlayClick={props.updateSubDialog}
            title='Update Payment Information'
            >
            <UpdatePaymentForm
                frequency={props.frequency}
                payment_frequency={props.payment_frequency}
                hourly={props.hourly}
                daily={props.daily}
                annual={props.annual}
                salary={props.salary}
                method={props.method}
                periodic={props.periodic}
                annualInput={props.annualInput}
                periodicInput={props.periodicInput}
                handleFormChange={props.handleFormChange} />
            <Navigation
                type='horizontal'>
                <Button
                    label='Save'
                    raised
                    primary
                    onClick={props.updatePaymentRequest} />
                <Button
                    label='Cancel'
                    raised
                    primary
                    onClick={props.updateSubDialog} />
            </Navigation>
        </Dialog>
    )
}

export default UpdatePaymentModal;