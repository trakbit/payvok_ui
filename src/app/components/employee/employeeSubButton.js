import React from 'react';
import { Button, Navigation } from 'react-toolbox';
import 'whatwg-fetch';

const EmployeeSubButton = (props) => {
    return (
        <Navigation
            type='horizontal'>
            <Button
                label='Add'
                raised
                primary
                icon='add_circle_outline'
                onClick={props.addSubDialog} />
            <Button
                label='Remove'
                raised
                primary
                icon='remove_circle_outline'
                onClick={props.removeSubButton} />
            <Button
                label='Update'
                raised
                primary
                icon='unarchive'
                onClick={props.updateSubDialog} />
        </Navigation>
    )
}

export default EmployeeSubButton;