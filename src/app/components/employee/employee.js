import React from 'react';
import { Card, Button, Dropdown, IconButton, Tabs, Tab, Table, Navigation, Dialog, section, Input, CardTitle } from 'react-toolbox';
import 'whatwg-fetch';
import _ from 'lodash';
import moment from 'moment';

import EmployeeFilters from './employeeFilters';
import EmployeeButton from './employeeButton';
import EmployeeSubButton from './employeeSubButton';
import AddEmployeeModal from './addEmployeeModal';
import UpdateEmployeeModal from './updateEmployeeModal';
import AddPaymentModal from './payment/addPaymentModal';
import UpdatePaymentModal from './payment/updatePaymentModal';

import { connect } from 'react-redux';
import {
    getEmployeeData,
    setCompanySelected,
    setFrequencySelected,
    setCompanyFilteredEmployees,
    setDepartmentFilteredEmployees,
    setFrequencyFilteredEmployees,
    getCompanyDataForEmployee,
    setSelectedDepartmentDropdown,
    addEmployeeDialog,
    handleDateChange,
    addEmployee,
    handleGenderChange,
    setEmployeeSelected,
    deleteEmployee,
    updateEmployeeDialog,
    loadEmployee,
    updateEmployee
} from '../../actions/employee';

import {
    setFrequency,
    setPaymentFrequency,
    setSalary,
    setAnnual,
    setPeriodic,
    setAnnualInput,
    setPeriodicInput,
    setMethod,
    setHourly,
    setDaily,
    getPaymentData,
    setSelectedPayment,
    addPayment,
    deletePayment,
    updatePayment
} from '../../actions/payment';

import {
    getAllDepartment,
    setDepartmentSelected,
    setDepartmentData
} from '../../actions/department';

export class Employee extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            activeAddPaymentDialog: false,
            activeUpdatePaymentDialog: false,
            index: 0
        }
    }

    componentDidMount() {
        this.props.getPaymentData();
        this.props.getCompanyDataForEmployee();
        this.props.getDepartmentData();

    };

    componentWillReceiveProps(nextProps) {
        ///********** */
        if (this.props.companySelected.value != nextProps.companySelected.value) {
            this.props.setCompanySelected(nextProps.companySelected)
            let selectedCompanyId = this.props.companySourceForEmployee[nextProps.companySelected.value].CompanyId;
            let companyFilteredEmployees = _.filter(this.props.employeeSource, { 'CompanyId': selectedCompanyId });
            this.props.setCompanyFilteredEmployees(companyFilteredEmployees);

            let selectedDepartmentId = this.props.allDepartment[this.props.departmentSelected.value].DepartmentId;
            let departmentFilteredEmployees = _.filter(companyFilteredEmployees, { 'DepartmentId': selectedDepartmentId });
            this.props.setDepartmentFilteredEmployees(departmentFilteredEmployees)


        }
        ///********** */ WHEN ANY CHANGE MADE TO EMPLOYEE DATA
        if (this.props.employeeSource != nextProps.employeeSource) {
            if (this.props.companySourceForEmployee.length === 0) {
            }
            else {
                let selectedCompanyId = this.props.companySourceForEmployee[this.props.companySelected.value].CompanyId;
                let selectedDepartments = _.filter(this.props.allDepartment, { 'CompanyId': selectedCompanyId });
                //Preparing Dropdown List of departments after filtering company
                _.forEach(selectedDepartments, (value, index) => value.value = index);

                let companyFilteredEmployees = _.filter(nextProps.employeeSource, { 'CompanyId': selectedCompanyId });
                this.props.setCompanyFilteredEmployees(companyFilteredEmployees)
                this.props.setDepartmentData(selectedDepartments)

                if (selectedDepartments.length > 0) {
                    let selectedDepartmentId = selectedDepartments[this.props.departmentSelected.value].DepartmentId;
                    let departmentFilteredEmployees = _.filter(companyFilteredEmployees, { 'DepartmentId': selectedDepartmentId });
                    this.props.setDepartmentFilteredEmployees(departmentFilteredEmployees);
                    this.props.setFrequencyFilteredEmployees(departmentFilteredEmployees);
                }
            }
        }
        ///********** */
        if (this.props.frequency != nextProps.frequency) {
            if (nextProps.frequency.value === 1) {
                this.props.setPaymentFrequency({ value: "Weekly" });
                if (this.props.salary.value == "annual") {
                    this.props.setPeriodic({ value: this.props.annual.value / 52 })
                }
                else if (this.props.salary.value == "periodic") {
                    this.props.setAnnual({ value: this.props.periodic.value * 52 })
                }
            }
            else if (nextProps.frequency.value === 2) {
                this.props.setPaymentFrequency({ value: "Fortnightly" });
                if (this.props.salary.value == "annual") {
                    this.props.setPeriodic({ value: this.props.annual.value / 26 })
                }
                else if (this.props.salary.value == "periodic") {
                    this.props.setAnnual({ value: this.props.periodic.value * 26 })
                }
            }
            else if (nextProps.frequency.value === 3) {
                this.props.setPaymentFrequency({ value: "4-Weekly" });
                if (this.props.salary.value == "annual") {
                    this.props.setPeriodic({ value: this.props.annual.value / 13 })
                }
                else if (this.props.salary.value == "periodic") {
                    this.props.setAnnual({ value: this.props.periodic.value * 13 })
                }
            }
            else if (nextProps.frequency.value === 4) {
                this.props.setPaymentFrequency({ value: "Monthly" });
                if (this.props.salary.value == "annual") {
                    this.props.setPeriodic({ value: this.props.annual.value / 12 })
                }
                else if (this.props.salary.value == "periodic") {
                    this.props.setAnnual({ value: this.props.periodic.value * 12 })
                }
            }
        }
        ///********** */
        if (this.props.periodic != nextProps.periodic) {
            if (this.props.salary.value == "periodic") {
                if (this.props.frequency.value === 1) {
                    this.props.setAnnual({ value: nextProps.periodic.value * 52 });
                }
                if (this.props.frequency.value === 2) {
                    this.props.setAnnual({ value: nextProps.periodic.value * 36 });
                }
                if (this.props.frequency.value === 3) {
                    this.props.setAnnual({ value: nextProps.periodic.value * 13 });
                }
                if (this.props.frequency.value === 4) {
                    this.props.setAnnual({ value: nextProps.periodic.value * 12 });
                }
            }
        }
        ///********** */
        if (this.props.annual != nextProps.annual) {
            if (this.props.salary.value == "annual") {
                if (this.props.frequency.value === 1) {
                    this.props.setPeriodic({ value: nextProps.annual.value / 52 });
                }
                if (this.props.frequency.value === 2) {
                    this.props.setPeriodic({ value: nextProps.annual.value / 36 });
                }
                if (this.props.frequency.value === 3) {
                    this.props.setPeriodic({ value: nextProps.annual.value / 13 });
                }
                if (this.props.frequency.value === 4) {
                    this.props.setPeriodic({ value: nextProps.annual.value / 12 });
                }
            }
        }
        ///********** */
        if (this.props.salary !== nextProps.salary) {
            if (nextProps.salary.value == "annual") {
                this.props.setPeriodicInput(this.props.periodicInput);
                this.props.setAnnualInput(this.props.annualInput);
            } else if (nextProps.salary.value == "periodic") {
                this.props.setPeriodicInput(this.props.periodicInput);
                this.props.setAnnualInput(this.props.annualInput);
            }
        }
        ///********** */ SET PAYMENT RECORD ON INITIAL LOAD
        if (this.props.companyFilteredEmployees !== nextProps.companyFilteredEmployees) {
            if (nextProps.companyFilteredEmployees.length > 0) {
                this.props.setEmployeeSelected([0])
                if (nextProps.companyFilteredEmployees[0].Payment !== null) {
                    let selectedPayment = _.filter(this.props.paymentSource, { 'EmployeeId': nextProps.companyFilteredEmployees[0].EmployeeId });
                    this.props.setSelectedPayment(selectedPayment)
                }
            }
            else {
                this.props.setSelectedPayment({})
            }
        }
        ///********** */ SET PAYMENT RECORD WHEN NEW PAYMENT ADDED
        if (this.props.paymentSource !== nextProps.paymentSource) {
            if (this.props.frequencyFilteredEmployees.length > 0) {
                let selectedPayment = _.filter(nextProps.paymentSource, { 'EmployeeId': this.props.frequencyFilteredEmployees[this.props.employeeSelected[0]].EmployeeId });
                this.props.setSelectedPayment(selectedPayment)
            }
        }
        ///********** */ SET DEPARTMENT FILTER WHEN COMPANY FILTER CHANGES
        if (this.props.companySelected !== nextProps.companySelected) {
            let departmentData = nextProps.departmentSource;
            _.forEach(departmentData, (value, index) => value.value = index);
            this.props.setDepartmentData(departmentData)
            this.props.setDepartmentSelected({ value: 0 })

            if (nextProps.departmentSource.length > 0) {
                let selectedDepartmentId = nextProps.departmentSource[0].DepartmentId
                let departmentFilteredEmployees = _.filter(this.props.employeeSource, { 'DepartmentId': selectedDepartmentId });
                this.props.setDepartmentFilteredEmployees(departmentFilteredEmployees)
                this.props.setFrequencyFilteredEmployees(departmentFilteredEmployees)

                ///********** */ SET PAYMENT WHEN COMPANY FILTER CHANGES
                if (departmentFilteredEmployees.length > 0) {
                    if (departmentFilteredEmployees[0].Payment !== null) {
                        let EmployeeId = departmentFilteredEmployees[0].EmployeeId;
                        let departmentFilteredPayment = _.filter(this.props.paymentSource, { 'EmployeeId': EmployeeId });
                        this.props.setSelectedPayment(departmentFilteredPayment);
                    }
                    else {
                        this.props.setSelectedPayment([]);
                    }
                }
                else {
                    this.props.setSelectedPayment([]);
                }
                ///****** */
            }


        }
        ///********** */SET PAYMENT AND EMPLOYEE WHEN DEPARTMENT FILTER CHANGES 
        if (this.props.departmentSelected !== nextProps.departmentSelected) {

            let frequencyFilteredEmployees;
            let employeeByFrequency = [];
            if (this.props.frequencySelected.value === 0) {
                frequencyFilteredEmployees = nextProps.departmentFilteredEmployees;
                this.props.setFrequencyFilteredEmployees(frequencyFilteredEmployees);
            } else {
                /**ADD EMPLOYEES WHOSE PAYMENT DATA IS NOT AVAILABLE IN THE API YET */
                let employeeIdByFrequency = [];
                let paymentByFrequency = _.filter(this.props.paymentSource, { Frequency: nextProps.frequencySelected.value })
                _.forEach(paymentByFrequency, (value) => {
                    if (value.Employee === null) {
                        employeeIdByFrequency.push(value.EmployeeId);
                    }
                });
                _.forEach(this.props.employeeSource, (employee) => {
                    _.forEach(employeeIdByFrequency, (employeeId) => {
                        if (employee.EmployeeId === employeeId) {
                            employeeByFrequency.push(employee)
                        }
                    })
                });
                /*** */
                frequencyFilteredEmployees = _.filter(nextProps.departmentFilteredEmployees, { Payment: { Frequency: this.props.frequencySelected.value } });
                frequencyFilteredEmployees.push(...employeeByFrequency);
                this.props.setFrequencyFilteredEmployees(frequencyFilteredEmployees);
            }

            if (frequencyFilteredEmployees.length > 0) {
                this.props.setEmployeeSelected([0]);
                let EmployeeId = frequencyFilteredEmployees[0].EmployeeId;
                let departmentFilteredPayment = _.filter(this.props.paymentSource, { 'EmployeeId': EmployeeId });
                this.props.setSelectedPayment(departmentFilteredPayment);
            }
            else {
                this.props.setSelectedPayment([]);
            }
        }
        //*********** */SET PAYMENT AND EMPLOYEE WHEN PAY FREQUENCY FILTER CHANGES
        if (this.props.frequencySelected !== nextProps.frequencySelected) {
            if (nextProps.departmentSource.length > 0) {
                let departmentId = this.props.departmentSource[nextProps.departmentSelected.value].DepartmentId;
                let departmentFilteredPayment = _.filter(this.props.paymentSource, { Employee: { 'DepartmentId': departmentId } });
                let frequencyFilteredEmployee = [];
                let frequencyFilteredPayment;
                let employeeByFrequency = [];

                if (nextProps.frequencySelected.value === 0) {
                    frequencyFilteredPayment = departmentFilteredPayment;
                } else {
                    /**ADD EMPLOYEES WHOSE PAYMENT DATA IS NOT AVAILABLE IN THE API YET */
                    let employeeIdByFrequency = [];
                    let paymentByFrequency = _.filter(this.props.paymentSource, { Frequency: nextProps.frequencySelected.value })
                    _.forEach(paymentByFrequency, (value) => {
                        if (value.Employee === null) {
                            employeeIdByFrequency.push(value.EmployeeId);
                        }
                    });
                    _.forEach(this.props.employeeSource, (employee) => {
                        _.forEach(employeeIdByFrequency, (employeeId) => {
                            if (employee.EmployeeId === employeeId) {
                                employeeByFrequency.push(employee)
                            }
                        })
                    });
                    /*** */
                    frequencyFilteredPayment = _.filter(departmentFilteredPayment, { Frequency: nextProps.frequencySelected.value });
                }

                if (frequencyFilteredPayment.length > 0) {
                    this.props.setEmployeeSelected([0]);
                }

                if (typeof frequencyFilteredPayment[0] == "undefined") {
                    this.props.setSelectedPayment([]);
                } else {
                    this.props.setSelectedPayment([frequencyFilteredPayment[0]]);

                    //***ADD EMPLOYEE WHICH HAVE PAYMENT DATA ADDED */
                    _.forEach(frequencyFilteredPayment, (value) => {
                        value.Employee.DateOfBirth = moment(value.Employee.DateOfBirth).format("MM/DD/YYYY");
                        value.Employee.Address = value.Employee.AddressLine1 + ' ' +
                            value.Employee.AddressLine2 + ' ' +
                            value.Employee.AddressLine3 + ' ' +
                            value.Employee.AddressLine4;
                        frequencyFilteredEmployee.push(value.Employee);
                        frequencyFilteredEmployee.push(...employeeByFrequency);
                    });

                    //***ADD EMPLOYEE WHICH HAVE NO PAYMENT DATA ADDED */
                    if (nextProps.frequencySelected.value === 0) {
                        _.forEach(nextProps.departmentFilteredEmployees, (value) => {
                            if (value.Payment === null) {
                                frequencyFilteredEmployee.push(value);
                            }
                            else {
                            }
                        })
                    }

                    this.props.setFrequencyFilteredEmployees(frequencyFilteredEmployee);
                }
            }

        }
        //*********** */
    }

    handleTabChange = (index) => {
        this.setState({ index });
    };

    handleEmployeeSelect = (employeeSelected) => {
        this.props.setEmployeeSelected(employeeSelected);
        let selectedEmployee = this.props.frequencyFilteredEmployees[employeeSelected[0]].EmployeeId;
        let selectedPayment = _.filter(this.props.paymentSource, { 'EmployeeId': selectedEmployee });
        this.props.setSelectedPayment(selectedPayment);
    }

    handleDepartmentFormChange = (value) => {
        this.props.setSelectedDepartmentDropdown({ value: value })
    };

    handleDateChange = (value) => {
        this.props.handleDateChange({ value: value })
    }

    handleGenderChange = (value) => {
        this.props.handleGenderChange({ value: value })
    }

    handleSubmit = (name, data) => {
        if (name == "addEmployee") {
            if (this.props.departmentSource.length > 0) {
                data.DateOfBirth = moment(this.props.dateOfBirth.value).format();
                data.DepartmentId = this.props.departmentSource[this.props.selectedDepartmentDropdown.value].DepartmentId;
                data.CompanyId = this.props.companySourceForEmployee[this.props.companySelected.value].CompanyId;
                data.Gender = this.props.gender.value;
                this.props.addEmployeeData(data);
            }
            this.props.addEmployeeDialog();
        }
        else if (name == "updateEmployee") {
            data.DateOfBirth = moment(this.props.dateOfBirth.value).format();
            data.Gender = this.props.gender.value;
            this.props.updateEmployee(data);
            this.props.updateEmployeeDialog();
        }
    }

    savePayment = () => {
        if (this.props.frequencyFilteredEmployees.length > 0) {
            let payment = {
                "EmployeeId": this.props.frequencyFilteredEmployees[this.props.employeeSelected].EmployeeId,
                "Frequency": this.props.frequency.value,
                "Method": this.props.method.value,
                "Annual": this.props.annual.value,
                "Periodic": this.props.periodic.value,
                "Hourly": this.props.hourly.value,
                "Daily": this.props.daily.value
            }
            this.props.addPayment(payment)
        }
        this.addSubDialog();
    }

    handleFormChange = (name, event) => {
        if (name == "frequency") {
            this.props.setFrequency({ value: event });
        }
        if (name == "salary") {
            this.props.setSalary({ value: event });
        }
        if (name == "annual") {
            this.props.setAnnual({ value: event });
        }
        if (name == "periodic") {
            this.props.setPeriodic({ value: event });
        }
        if (name == "method") {
            this.props.setMethod({ value: event });
        }
        if (name == "hourly") {
            this.props.setHourly({ value: event });
        }
        if (name == "daily") {
            this.props.setDaily({ value: event });
        }
    }

    handleCompanyChange = (value) => {
        this.props.setCompanySelected({ value: value })
        let selectedCompanyId = this.props.companySourceForEmployee[value].CompanyId;
        let dropdownDepartments = _.filter(this.props.allDepartment, { 'CompanyId': selectedCompanyId });
        this.props.setDepartmentData(dropdownDepartments)
    };

    handleDepartmentChange = (value) => {
        this.props.setDepartmentSelected({ value: value })
        let selectedDepartmentId = this.props.departmentSource[value].DepartmentId;
        let departmentFilteredEmployees = _.filter(this.props.companyFilteredEmployees, { 'DepartmentId': selectedDepartmentId });
        this.props.setDepartmentFilteredEmployees(departmentFilteredEmployees)
    };

    handleFrequencyChange = (value) => {
        this.props.setFrequencySelected({ value: value })
        if (value === 0) {
            this.props.setFrequencyFilteredEmployees(this.props.departmentFilteredEmployees);
        }
        else {
            let frequencyFilteredEmployees = _.filter(this.props.departmentFilteredEmployees, { 'Payment': { 'Frequency': value } });
            this.props.setFrequencyFilteredEmployees(frequencyFilteredEmployees);
        }
    }

    addEmployeeDialog = () => {
        this.setState({ activeAddEmployeeDialog: !this.state.activeAddEmployeeDialog });
    };

    removeEmployee = () => {
        if (this.props.frequencyFilteredEmployees.length > 0) {
            let EmployeeId = this.props.frequencyFilteredEmployees[this.props.employeeSelected].EmployeeId;
            this.props.deleteEmployee(EmployeeId);
        }
    }

    updateEmployeeDialogAndLoadEmployee = () => {
        if (this.props.frequencyFilteredEmployees.length > 0) {
            let loadedEmployee = this.props.frequencyFilteredEmployees[this.props.employeeSelected];
            this.props.setSelectedDepartmentDropdown(loadedEmployee.Department.Department_Name);
            this.props.handleDateChange({ value: new Date(loadedEmployee.DateOfBirth) });
            this.props.handleGenderChange({ value: loadedEmployee.Gender });
            this.props.loadEmployee(loadedEmployee);
            this.props.updateEmployeeDialog();
        }
    }

    addSubDialog = () => {
        if (this.state.index === 0) {
            if (this.props.selectedPayment.length === 0) {
                this.setState({ activeAddPaymentDialog: !this.state.activeAddPaymentDialog });
            }
        }
    };

    removeSubButton = () => {
        if (this.state.index === 0) {
            if (this.props.frequencyFilteredEmployees.length > 0) {
                let EmployeeId = this.props.companyFilteredEmployees[this.props.employeeSelected[0]].EmployeeId;
                this.props.deletePayment(EmployeeId);
            }
        }

    }

    updateSubDialog = () => {
        if (this.state.index === 0) {
            this.updatePayment();
        }
    };

    updatePayment = () => {
        if (this.props.selectedPayment.length === 0) {
        } else {
            this.setState({ activeUpdatePaymentDialog: !this.state.activeUpdatePaymentDialog });
            this.props.setAnnual({ value: this.props.selectedPayment[0].Annual });
            this.props.setPeriodic({ value: this.props.selectedPayment[0].Periodic });
            this.props.setFrequency({ value: this.props.selectedPayment[0].Frequency });
            this.props.setPaymentFrequency({ value: this.props.selectedPayment[0].Payment_Frequency });
            this.props.setHourly({ value: this.props.selectedPayment[0].Hourly });
            this.props.setDaily({ value: this.props.selectedPayment[0].Daily });
            this.props.setMethod({ value: this.props.selectedPayment[0].Method });
        }
    }

    updatePaymentRequest = () => {
        let data = {
            "EmployeeId": this.props.companyFilteredEmployees[this.props.employeeSelected[0]].EmployeeId,
            "Frequency": this.props.frequency.value,
            "Method": this.props.method.value,
            "Annual": this.props.annual.value,
            "Periodic": this.props.periodic.value,
            "Hourly": this.props.hourly.value,
            "Daily": this.props.daily.value
        }
        this.props.updatePayment(data);
        this.setState({ activeUpdatePaymentDialog: !this.state.activeUpdatePaymentDialog });
    }



    render() {
        return (
            <div style={{ flex: 1, overflowY: 'auto', padding: '1.8rem' }}>
                <EmployeeFilters
                    companySourceForEmployee={this.props.companySourceForEmployee}
                    departmentSource={this.props.departmentSource}
                    handleCompanyChange={this.handleCompanyChange}
                    handleDepartmentChange={this.handleDepartmentChange}
                    handleFrequencyChange={this.handleFrequencyChange}
                    customItem={this.customItem}
                    customDepartment={this.customDepartment}
                    companySelected={this.props.companySelected}
                    departmentSelected={this.props.departmentSelected}
                    frequencySelected={this.props.frequencySelected} />

                <EmployeeButton
                    addEmployeeDialog={this.props.addEmployeeDialog}
                    removeEmployee={this.removeEmployee}
                    updateEmployeeDialogAndLoadEmployee={this.updateEmployeeDialogAndLoadEmployee} />

                <div style={{ padding: '1em' }}>
                    <Card style={{
                        'backgroundColor': 'WhiteSmoke',
                        'overflow': 'scroll',
                        'height': '30em'
                    }}>
                        <CardTitle title="Employee" />
                        <Table
                            multiSelectable={false}
                            onSelect={this.handleEmployeeSelect}
                            selected={this.props.employeeSelected}
                            model={EmployeeModel}
                            source={this.props.frequencyFilteredEmployees}
                            />
                    </Card>
                </div>

                <AddEmployeeModal
                    activeAddEmployeeDialog={this.props.activeAddEmployeeDialog}
                    departmentSource={this.props.departmentSource}
                    handleDateChange={this.handleDateChange}
                    handleGenderChange={this.handleGenderChange}
                    handleDepartmentFormChange={this.handleDepartmentFormChange}
                    selectedDepartmentDropdown={this.props.selectedDepartmentDropdown}
                    addEmployeeDialog={this.props.addEmployeeDialog}
                    dateOfBirth={this.props.dateOfBirth}
                    gender={this.props.gender}
                    onSubmit={this.handleSubmit.bind(this, 'addEmployee')} />

                <UpdateEmployeeModal
                    activeUpdateEmployeeDialog={this.props.activeUpdateEmployeeDialog}
                    updateEmployeeDialog={this.props.updateEmployeeDialog}
                    loadedEmployee={this.props.loadedEmployee}
                    departmentSource={this.props.departmentSource}
                    selectedDepartmentDropdown={this.props.selectedDepartmentDropdown}
                    handleDepartmentFormChange={this.handleDepartmentFormChange}
                    handleDateChange={this.handleDateChange}
                    handleGenderChange={this.handleGenderChange}
                    dateOfBirth={this.props.dateOfBirth}
                    gender={this.props.gender}
                    onSubmit={this.handleSubmit.bind(this, 'updateEmployee')} />

                <EmployeeSubButton
                    addSubDialog={this.addSubDialog}
                    removeSubButton={this.removeSubButton}
                    updateSubDialog={this.updateSubDialog} />

                <div style={{ padding: '1em' }}>
                    <Card style={{ 'backgroundColor': 'WhiteSmoke' }}>
                        <Tabs
                            index={this.state.index}
                            onChange={this.handleTabChange}>
                            <Tab label='Payment'>
                                <Table
                                    selectable={false}
                                    model={PaymentModel}
                                    source={this.props.selectedPayment}
                                    />
                            </Tab>
                            <Tab label='Employment'>Employment</Tab>
                        </Tabs>
                    </Card>
                </div>

                <AddPaymentModal
                    activeAddPaymentDialog={this.state.activeAddPaymentDialog}
                    addSubDialog={this.addSubDialog}
                    frequency={this.props.frequency}
                    payment_frequency={this.props.payment_frequency}
                    handleFormChange={this.handleFormChange}
                    getCompanyData={this.getCompanyData}
                    method={this.props.method}
                    annual={this.props.annual}
                    daily={this.props.daily}
                    hourly={this.props.hourly}
                    salary={this.props.salary}
                    periodic={this.props.periodic}
                    annualInput={this.props.annualInput}
                    periodicInput={this.props.periodicInput}
                    savePayment={this.savePayment} />

                <UpdatePaymentModal
                    activeUpdatePaymentDialog={this.state.activeUpdatePaymentDialog}
                    updateSubDialog={this.updateSubDialog}
                    frequency={this.props.frequency}
                    payment_frequency={this.props.payment_frequency}
                    handleFormChange={this.handleFormChange}
                    getCompanyData={this.getCompanyData}
                    method={this.props.method}
                    annual={this.props.annual}
                    daily={this.props.daily}
                    hourly={this.props.hourly}
                    salary={this.props.salary}
                    periodic={this.props.periodic}
                    annualInput={this.props.annualInput}
                    periodicInput={this.props.periodicInput}
                    updatePaymentRequest={this.updatePaymentRequest} />

            </div>
        )
    }
}

const EmployeeModel = {
    EmployeeId: { type: String },
    FirstName: { type: String },
    LastName: { type: String },
    MiddleName: { type: String },
    Address: { type: String },
    DateOfBirth: { type: String },
    Gender: { type: String },
    Email: { type: String }
};

const PaymentModel = {
    Payment_Frequency: { type: String },
    Method: { type: String },
    Annual: { type: String },
    Periodic: { type: String },
    Hourly: { type: String },
    Daily: { type: String }
};

const mapStateToProps = (state) => {
    return {
        //PAYMENT
        paymentSource: state.paymentSource,
        selectedPayment: state.selectedPayment,
        method: state.method,
        salary: state.salary,
        payment_frequency: state.payment_frequency,
        frequency: state.frequency,
        hourly: state.hourly,
        daily: state.daily,
        annual: state.annual,
        periodic: state.periodic,
        annualInput: state.annualInput,
        periodicInput: state.periodicInput,
        //EMPLOYEE
        loadedEmployee: state.loadedEmployee,
        employeeSelected: state.employeeSelected,
        gender: state.gender,
        dateOfBirth: state.dateOfBirth,
        companySourceForEmployee: state.companySourceForEmployee,
        departmentFilteredEmployees: state.departmentFilteredEmployees,
        frequencyFilteredEmployees: state.frequencyFilteredEmployees,
        employeeSource: state.employeeSource,
        allDepartment: state.allDepartment,
        departmentSource: state.departmentSource,
        //EMPLOYEE FILTERS       
        companySelected: state.companySelected,
        departmentSelected: state.departmentSelected,
        frequencySelected: state.frequencySelected,
        //***** */
        companyFilteredEmployees: state.companyFilteredEmployees,
        selectedDepartmentDropdown: state.selectedDepartmentDropdown,
        activeAddEmployeeDialog: state.activeAddEmployeeDialog,
        activeUpdateEmployeeDialog: state.activeUpdateEmployeeDialog
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setDateOfBirth: (dateOfBirth) => dispatch(setDateOfBirth(dateOfBirth)),
        addEmployeeData: (data) => dispatch(addEmployee(data)),
        handleDateChange: (value) => dispatch(handleDateChange(value)),
        handleGenderChange: (value) => dispatch(handleGenderChange(value)),
        getCompanyDataForEmployee: () => dispatch(getCompanyDataForEmployee()),
        getEmployeeData: () => dispatch(getEmployeeData()),
        getDepartmentData: () => dispatch(getAllDepartment()),
        setDepartmentData: (departmentSource) => dispatch(setDepartmentData(departmentSource)),
        setCompanySelected: (companySelected) => dispatch(setCompanySelected(companySelected)),
        setDepartmentSelected: (departmentSelected) => dispatch(setDepartmentSelected(departmentSelected)),
        setFrequencySelected: (frequencySelected) => dispatch(setFrequencySelected(frequencySelected)),
        setCompanyFilteredEmployees: (companyFilteredEmployees) => dispatch(setCompanyFilteredEmployees(companyFilteredEmployees)),
        setDepartmentFilteredEmployees: (departmentFilteredEmployees) => dispatch(setDepartmentFilteredEmployees(departmentFilteredEmployees)),
        setFrequencyFilteredEmployees: (frequencyFilteredEmployees) => dispatch(setFrequencyFilteredEmployees(frequencyFilteredEmployees)),
        setEmployeeSelected: (employeeSelected) => dispatch(setEmployeeSelected(employeeSelected)),
        setSelectedDepartmentDropdown: (value) => dispatch(setSelectedDepartmentDropdown(value)),
        addEmployeeDialog: (activeAddEmployeeDialog) => dispatch(addEmployeeDialog(activeAddEmployeeDialog)),
        deleteEmployee: (EmployeeId) => dispatch(deleteEmployee(EmployeeId)),
        loadEmployee: (loadedEmployee) => dispatch(loadEmployee(loadedEmployee)),
        updateEmployeeDialog: (activeUpdateEmployeeDialog) => dispatch(updateEmployeeDialog(activeUpdateEmployeeDialog)),
        updateEmployee: (employee) => dispatch(updateEmployee(employee)),
        setFrequency: (frequency) => dispatch(setFrequency(frequency)),
        setPaymentFrequency: (payment_frequency) => dispatch(setPaymentFrequency(payment_frequency)),
        setSalary: (salary) => dispatch(setSalary(salary)),
        setPeriodic: (periodic) => dispatch(setPeriodic(periodic)),
        setAnnual: (annual) => dispatch(setAnnual(annual)),
        setAnnualInput: (annualInput) => dispatch(setAnnualInput(annualInput)),
        setPeriodicInput: (periodicInput) => dispatch(setPeriodicInput(periodicInput)),
        setMethod: (method) => dispatch(setMethod(method)),
        setHourly: (hourly) => dispatch(setHourly(hourly)),
        setDaily: (daily) => dispatch(setDaily(daily)),
        getPaymentData: () => dispatch(getPaymentData()),
        setSelectedPayment: (selectedPayment) => dispatch(setSelectedPayment(selectedPayment)),
        addPayment: (payment) => dispatch(addPayment(payment)),
        deletePayment: (EmployeeId) => dispatch(deletePayment(EmployeeId)),
        updatePayment: (data) => dispatch(updatePayment(data))
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Employee);