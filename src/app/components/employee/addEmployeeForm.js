import React from 'react';
import { Button, Navigation, section, Input, DatePicker, Dropdown } from 'react-toolbox';
import 'whatwg-fetch';
import { Field, reduxForm } from 'redux-form';

const AddEmployeeForm = (props) => {

    const {handleSubmit} = props;

    const email = value =>
        value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
            'Invalid email address' : undefined;
    const required = value => value ? undefined : 'Required';

    const gender = [
        { value: 'male', label: 'Male' },
        { value: 'female', label: 'Female' }
    ];

    const selectDepartmentField = ({ select, meta: { touched, error, warning } }) => (
        <div>
            <Dropdown {...select}
                icon='business'
                auto={false}
                source={props.departmentSource}
                label='Select Department'
                template={customItem}
                value={props.selectedDepartmentDropdown.value}
                onChange={props.handleDepartmentFormChange} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const firstNameField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='account_circle'
                label='First Name'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const lastNameField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='account_circle'
                label='Last Name'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const middleNameField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='account_circle'
                label='Middle Name'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const addressLine1Field = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='place'
                label='Address'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const addressLine2Field = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='place'
                label='Address'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );
    const addressLine3Field = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='place'
                label='Address'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );
    const addressLine4Field = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='place'
                label='Address'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );


    const dateOfBirthField = ({  meta: { touched, error, warning } }) => (
        <div>
            <DatePicker
                icon="today"
                label="Date Of Birth"
                value={props.dateOfBirth.value}
                onChange={props.handleDateChange}
                />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const genderField = ({ select, meta: { touched, error, warning } }) => (
        <div>
            <Dropdown {...select}
                icon='wc'
                auto={false}
                source={gender}
                label='Gender'
                value={props.gender.value}
                onChange={props.handleGenderChange} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    const emailField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='mail'
                label='Email'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );


    return (
        <section>
            <form onSubmit={handleSubmit}>
                <Field
                    component={selectDepartmentField}
                    name="DepartmentId"
                    value={props.selectedDepartmentDropdown} />
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <Field
                                    component={firstNameField}
                                    name="FirstName"
                                    validate={required} />
                            </td>
                            <td>
                                <Field
                                    component={middleNameField}
                                    name="MiddleName"
                                    validate={required} />
                            </td>
                            <td>
                                <Field
                                    component={lastNameField}
                                    name="LastName"
                                    validate={required} />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <Field
                    component={addressLine1Field}
                    name="AddressLine1"
                    validate={required} />
                <Field
                    component={addressLine2Field}
                    name="AddressLine2"
                    validate={required} />
                <Field
                    component={addressLine3Field}
                    name="AddressLine3"
                    validate={required} />
                <Field
                    component={addressLine4Field}
                    name="AddressLine4"
                    validate={required} />
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <Field
                                    component={dateOfBirthField}
                                    value={props.dateOfBirth.value}
                                    type="text"
                                    name="DateOfBirth" />
                            </td>
                            <td>
                                <Field
                                    component={genderField}
                                    name="Gender"
                                    value={props.gender.value} />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <Field
                    component={emailField}
                    name="Email"
                    type="email"
                    validate={[email, required]} />
            </form>
            <Navigation type='horizontal'>
                <Button
                    label='Save'
                    raised
                    primary
                    onClick={props.handleSubmit} />
                <Button
                    label='Cancel'
                    raised
                    primary
                    onClick={props.addEmployeeDialog} />
            </Navigation>
        </section>
    );
}

const customItem = (item) => {
    const containerStyle = {
        display: 'flex',
        flexDirection: 'row'
    };

    const contentStyle = {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 2
    };

    return (
        <div style={containerStyle}>
            <div style={contentStyle}>
                <strong>{item.DepartmentId}</strong>
                <small>{item.Department_Name}</small>
            </div>
        </div>
    );
}


export default reduxForm({
    form: 'employee',  // a unique identifier for this form
})(AddEmployeeForm)