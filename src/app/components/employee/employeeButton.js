import React from 'react';
import { Button, Navigation } from 'react-toolbox';
import 'whatwg-fetch';

const EmployeeButton = (props) => {
    return (
        <Navigation
            type='horizontal'>
            <Button
                label='Add'
                raised
                primary
                onClick={props.addEmployeeDialog}
                icon='add_circle_outline' />
            <Button
                label='Remove'
                raised
                primary
                onClick={props.removeEmployee}
                icon='remove_circle_outline' />
            <Button
                label='Update'
                raised
                primary
                onClick={props.updateEmployeeDialogAndLoadEmployee}
                icon='unarchive' />
            <Button
                label='P45 Part 3'
                raised
                primary
                icon='description' />
            <Button
                label='P46'
                raised
                primary
                icon='description' />
            <Button
                label='P45'
                raised
                accent
                icon='description' />
        </Navigation>
    );
}

export default EmployeeButton;