import React from 'react';
import { Button, Navigation, section, Input, DatePicker, Dropdown } from 'react-toolbox';
import 'whatwg-fetch';
import { Field, reduxForm } from 'redux-form';

class UpdateEmployeeForm extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.initialize(this.props.loadedEmployee);
    }


    email = value =>
        value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
            'Invalid email address' : undefined;
    required = value => value ? undefined : 'Required';

    gender = [
        { value: 'male', label: 'Male' },
        { value: 'female', label: 'Female' }
    ];

    selectDepartmentField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='business'
                placeholder={label}
                label='Selected Department'
                type={type}
                value={this.props.loadedEmployee.Department.Department_Name}
                disabled />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    firstNameField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='account_circle'
                label='First Name'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    lastNameField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='account_circle'
                label='Last Name'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    middleNameField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='account_circle'
                label='Middle Name'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    addressLine1Field = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='place'
                label='Address'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    addressLine2Field = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='place'
                label='Address'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );
    addressLine3Field = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='place'
                label='Address'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );
    addressLine4Field = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='place'
                label='Address'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );


    dateOfBirthField = ({ meta: { touched, error, warning } }) => (
        <div>
            <DatePicker
                icon="today"
                label="Date Of Birth"
                value={this.props.dateOfBirth.value}
                onChange={this.props.handleDateChange}
                />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    genderField = ({ select, meta: { touched, error, warning } }) => (
        <div>
            <Dropdown {...select}
                icon='wc'
                auto={false}
                source={this.gender}
                label='Gender'
                value={this.props.gender.value}
                onChange={this.props.handleGenderChange} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    emailField = ({ input, label, type, meta: { touched, error, warning } }) => (
        <div>
            <Input {...input}
                icon='mail'
                label='Email'
                placeholder={label}
                type={type} />
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    );

    render() {
        return (
            <section>
                <form onSubmit={this.handleSubmit}>
                    <Field
                        component={this.selectDepartmentField}
                        name={'this.props.selectedDepartmentDropdown.value'}
                        value={this.props.loadedEmployee.Department.Department_Name} />
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <Field
                                        component={this.firstNameField}
                                        name="FirstName"
                                        validate={this.required} />
                                </td>
                                <td>
                                    <Field
                                        component={this.middleNameField}
                                        name="MiddleName"
                                        validate={this.required} />
                                </td>
                                <td>
                                    <Field
                                        component={this.lastNameField}
                                        name="LastName"
                                        validate={this.required} />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <Field
                        component={this.addressLine1Field}
                        name="AddressLine1"
                        validate={this.required} />
                    <Field
                        component={this.addressLine2Field}
                        name="AddressLine2"
                        validate={this.required} />
                    <Field
                        component={this.addressLine3Field}
                        name="AddressLine3"
                        validate={this.required} />
                    <Field
                        component={this.addressLine4Field}
                        name="AddressLine4"
                        validate={this.required} />
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <Field
                                        component={this.dateOfBirthField}
                                        value={this.props.dateOfBirth.value}
                                        type="text"
                                        name="DateOfBirth" />
                                </td>
                                <td>
                                    <Field
                                        component={this.genderField}
                                        name="Gender"
                                        value={this.props.gender.value} />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <Field
                        component={this.emailField}
                        name="Email"
                        type="email"
                        validate={[this.email, this.required]} />
                </form>
                <Navigation type='horizontal'>
                    <Button
                        label='Save'
                        raised
                        primary
                        onClick={this.props.handleSubmit} />
                    <Button
                        label='Cancel'
                        raised
                        primary
                        onClick={this.props.updateEmployeeDialog} />
                </Navigation>
            </section>
        );
    }

}

export default reduxForm({
    form: 'employee',  // a unique identifier for this form
})(UpdateEmployeeForm)