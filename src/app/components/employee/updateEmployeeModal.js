import React from 'react';
import { Dialog } from 'react-toolbox';
import 'whatwg-fetch';
import UpdateEmployeeForm from './updateEmployeeForm';

export const UpdateEmployeeModal = (props) => {
    return (
        <Dialog
            active={props.activeUpdateEmployeeDialog}
            onEscKeyDown={props.updateEmployeeDialog}
            onOverlayClick={props.updateEmployeeDialog}
            title='Update Employee Information'
            >
            <UpdateEmployeeForm
                loadedEmployee={props.loadedEmployee}
                departmentSource={props.departmentSource}
                selectedDepartmentDropdown={props.selectedDepartmentDropdown}
                dateOfBirth={props.dateOfBirth}
                gender={props.gender}
                handleDateChange={props.handleDateChange}
                handleGenderChange={props.handleGenderChange}
                updateEmployeeDialog={props.updateEmployeeDialog}
                onSubmit={props.onSubmit} />
        </Dialog>
    )
}

export default UpdateEmployeeModal;