import React from 'react';
import { Dialog, } from 'react-toolbox';
import 'whatwg-fetch';
import AddEmployeeForm from './addEmployeeForm';

const AddEmployeeModal = (props) => {
    return (
        <Dialog
            active={props.activeAddEmployeeDialog}
            onEscKeyDown={props.addEmployeeDialog}
            onOverlayClick={props.addEmployeeDialog}
            title='Add Employee Information'>
            <AddEmployeeForm
                onSubmit={props.onSubmit}
                activeAddEmployeeDialog={props.activeAddEmployeeDialog}
                departmentSource={props.departmentSource}
                dateOfBirth={props.dateOfBirth}
                gender={props.gender}
                handleGenderChange={props.handleGenderChange}
                handleDateChange={props.handleDateChange}
                handleDepartmentFormChange={props.handleDepartmentFormChange}
                selectedDepartmentDropdown={props.selectedDepartmentDropdown}
                addEmployeeDialog={props.addEmployeeDialog} />
        </Dialog>
    )
}

export default AddEmployeeModal;