import React from 'react';
import { Dropdown } from 'react-toolbox';
import 'whatwg-fetch';

const EmployeeFilters = (props) => {
    return (
        <table>
            <tbody>
                <tr>
                    <td style={{ width: 200 }}>
                        <Dropdown
                            icon='business'
                            auto={false}
                            source={props.companySourceForEmployee}
                            onChange={props.handleCompanyChange}
                            label='Select Company'
                            template={customCompany}
                            value={props.companySelected.value} />
                        &nbsp;
                        &nbsp;
                    </td>
                    <td style={{ width: 200 }}>
                        <Dropdown
                            icon='business'
                            auto={false}
                            source={props.departmentSource}
                            onChange={props.handleDepartmentChange}
                            label='Select Department'
                            template={customDepartment}
                            value={props.departmentSelected.value} />
                        &nbsp;
                        &nbsp;
                    </td>
                    <td style={{ width: 200 }}>
                        <Dropdown
                            icon='business'
                            auto={false}
                            source={frequencies}
                            onChange={props.handleFrequencyChange}
                            label='Select Pay Frequency'
                            template={customPay}
                            value={props.frequencySelected.value} />
                        &nbsp;
                        &nbsp;
                    </td>
                </tr>
            </tbody>
        </table>
    )
}

const customCompany = (item) => {
    const containerStyle = {
        display: 'flex',
        flexDirection: 'row'
    };

    const contentStyle = {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 2
    };

    return (
        <div style={containerStyle}>
            <div style={contentStyle}>
                <strong>{item.Name}</strong>
            </div>
        </div>
    );
}

const customDepartment = (item) => {
    const containerStyle = {
        display: 'flex',
        flexDirection: 'row'
    };

    const contentStyle = {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 2
    };

    return (
        <div style={containerStyle}>
            <div style={contentStyle}>
                <strong>{item.Department_Name}</strong>
            </div>
        </div>
    );
}

const customPay = (item) => {
    const containerStyle = {
        display: 'flex',
        flexDirection: 'row'
    };

    const contentStyle = {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 2
    };

    return (
        <div style={containerStyle}>
            <div style={contentStyle}>
                <strong>{item.frequency}</strong>
            </div>
        </div>
    );
}



const frequencies = [
    { value: 0, frequency: 'All' },
    { value: 1, frequency: 'Weekly' },
    { value: 2, frequency: 'Fortnightly' },
    { value: 3, frequency: '4-Weekly' },
    { value: 4, frequency: 'Monthly' }
];

export default EmployeeFilters;